# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    différents QDialogs de configuration.
"""


# importation des modules perso :
import utils, utils_functions, pave

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui



class PenPropertiesDlg(QtWidgets.QDialog):
    """
    QDialog de configuration du style
    de lignes du contour.
    """
    def __init__(self, parent=None):
        super(PenPropertiesDlg, self).__init__(parent)
        self.main = parent
        
        self.setMinimumWidth(300)
        self.pen = QtGui.QPen()

        self.penWidthSpinBox = QtWidgets.QSpinBox()
        self.penWidthSpinBox.setRange(1, 20)

        self.penWidthLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate('main', 'Pen &Width:'))
        self.penWidthLabel.setBuddy(self.penWidthSpinBox)

        self.penStyleComboBox = QtWidgets.QComboBox()
        self.penStyleComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Solid'), 
            QtCore.Qt.SolidLine)
        self.penStyleComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Dash'), 
            QtCore.Qt.DashLine)
        self.penStyleComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Dot'), 
            QtCore.Qt.DotLine)
        self.penStyleComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Dash Dot'), 
            QtCore.Qt.DashDotLine)
        self.penStyleComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Dash Dot Dot'), 
            QtCore.Qt.DashDotDotLine)
        self.penStyleComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'None'), 
            QtCore.Qt.NoPen)

        self.penStyleLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate('main', '&Pen Style:'))
        self.penStyleLabel.setBuddy(self.penStyleComboBox)

        self.penJoinComboBox = QtWidgets.QComboBox()
        self.penJoinComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Miter'), 
            QtCore.Qt.MiterJoin)
        self.penJoinComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Bevel'), 
            QtCore.Qt.BevelJoin)
        self.penJoinComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Round'), 
            QtCore.Qt.RoundJoin)

        self.penJoinLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate('main', 'Pen &Join:'))
        self.penJoinLabel.setBuddy(self.penJoinComboBox)

        self.colorButton = QtWidgets.QPushButton(
            QtWidgets.QApplication.translate('main', '&Color'))
        self.colorLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate('main', 'Pen Color:'))

        self.okButton = QtWidgets.QPushButton(
            QtWidgets.QApplication.translate('main', '&OK'))
        self.cancelButton = QtWidgets.QPushButton(
            QtWidgets.QApplication.translate('main', 'C&ancel'))

        checkBoxLayout = QtWidgets.QHBoxLayout()
        checkBoxLayout.addWidget(self.okButton)
        checkBoxLayout.addWidget(self.cancelButton)

        mainLayout = QtWidgets.QGridLayout()
        mainLayout.addWidget(self.penWidthLabel, 1, 0)
        mainLayout.addWidget(self.penWidthSpinBox, 1, 1)
        mainLayout.addWidget(self.penStyleLabel, 2, 0)
        mainLayout.addWidget(self.penStyleComboBox, 2, 1)
        mainLayout.addWidget(self.penJoinLabel, 3, 0)
        mainLayout.addWidget(self.penJoinComboBox, 3, 1)
        mainLayout.addWidget(self.colorLabel, 4, 0)
        mainLayout.addWidget(self.colorButton, 4, 1)
        mainLayout.addLayout(checkBoxLayout, 7, 0, 1, 2)
        self.setLayout(mainLayout)

        self.penWidthSpinBox.valueChanged.connect(self.penChanged)
        self.penStyleComboBox.activated.connect(self.penChanged)
        self.penJoinComboBox.activated.connect(self.penChanged)
        self.colorButton.clicked.connect(self.choosecolor)
        self.okButton.clicked.connect(self.accept)
        self.cancelButton.clicked.connect(self.reject)

        self.penWidthSpinBox.setValue(self.main.monPave.lineWidth)
        i =  self.main.monPave.lineStyle - 1
        if i == -1:
            i = 5
        self.penStyleComboBox.setCurrentIndex(i)
        if self.main.monPave.lineJoin == QtCore.Qt.BevelJoin:
            i = 1
        elif self.main.monPave.lineJoin == QtCore.Qt.RoundJoin:
            i = 2
        else:
            i = 0
        self.penJoinComboBox.setCurrentIndex(i)
        self.penChanged()
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Line Style'))

    def penChanged(self):
        color = self.main.monPave.lineColor
        color = QtGui.QColor(color[0], color[1], color[2], color[3])
        width = self.penWidthSpinBox.value()
        style = QtCore.Qt.PenStyle(self.penStyleComboBox.itemData(
            self.penStyleComboBox.currentIndex(), QtCore.Qt.UserRole))
        join = QtCore.Qt.PenJoinStyle(self.penJoinComboBox.itemData(
            self.penJoinComboBox.currentIndex(), QtCore.Qt.UserRole))
        cap = QtCore.Qt.FlatCap
        self.pen = QtGui.QPen(color, width, style, cap, join)

    def choosecolor(self):
        color = self.main.monPave.lineColor
        color = QtGui.QColor(color[0], color[1], color[2], color[3])
        color = QtWidgets.QColorDialog.getColor(
            color, self, '', QtWidgets.QColorDialog.ShowAlphaChannel)
        if color.isValid():
            self.main.monPave.lineColor[0] = color.red()
            self.main.monPave.lineColor[1] = color.green()
            self.main.monPave.lineColor[2] = color.blue()
            self.main.monPave.lineColor[3] = color.alpha()
            self.main.updateTitle(mustSave=True)


class PavageConfigDlg(QtWidgets.QDialog):
    """
    QDialog de configuration du pavage.
    Nombre de lignes et de colonnes.
    """
    def __init__(self, parent=None):
        super(PavageConfigDlg, self).__init__(parent)
        self.main = parent

        self.setMinimumWidth(400)
        self.nbXSpinBox = QtWidgets.QSpinBox()
        self.nbXSpinBox.setRange(1, 50)
        self.nbXLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate('main', 'Cols:'))
        self.nbXLabel.setBuddy(self.nbXSpinBox)

        self.nbYSpinBox = QtWidgets.QSpinBox()
        self.nbYSpinBox.setRange(1, 50)
        self.nbYLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate('main', 'Rows:'))
        self.nbYLabel.setBuddy(self.nbYSpinBox)

        self.okButton = QtWidgets.QPushButton(
            QtWidgets.QApplication.translate('main', '&OK'))
        self.cancelButton = QtWidgets.QPushButton(
            QtWidgets.QApplication.translate('main', 'C&ancel'))
        checkBoxLayout = QtWidgets.QHBoxLayout()
        checkBoxLayout.addWidget(self.okButton)
        checkBoxLayout.addWidget(self.cancelButton)

        mainLayout = QtWidgets.QGridLayout()
        mainLayout.addWidget(self.nbXLabel, 1, 0)
        mainLayout.addWidget(self.nbXSpinBox, 1, 1)
        mainLayout.addWidget(self.nbYLabel, 2, 0)
        mainLayout.addWidget(self.nbYSpinBox, 2, 1)
        mainLayout.addLayout(checkBoxLayout, 7, 0, 1, 2)
        self.setLayout(mainLayout)

        self.okButton.clicked.connect(self.accept)
        self.cancelButton.clicked.connect(self.reject)

        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Tiling Config'))


class LanguageChooserDlg(QtWidgets.QDialog):
    """
    QDialog pour sélectionner une autre langue.
    """
    def __init__(self, parent=None):
        super(LanguageChooserDlg, self).__init__(parent)
        self.main = parent

        self.languagesComboBox = QtWidgets.QComboBox()
        self.translator = QtCore.QTranslator()
        self.actualLang = [parent.locale, '', '']

        langs = []
        for lang in parent.translations:
            langs.append(lang)
        langs.sort()
        for lang in langs:
            (qmFile, languageName) = parent.translations[lang]
            self.languagesComboBox.addItem(
                languageName, 
                (lang, qmFile))
            if lang == self.actualLang[0]:
                self.actualLang = [
                    lang, 
                    languageName, 
                    qmFile]
        # préselection :
        index = self.languagesComboBox.findText(self.actualLang[1])
        if index > -1:
            self.languagesComboBox.setCurrentIndex(index)
        self.languagesComboBox.activated.connect(
            self.languageChanged)

        self.okButton = QtWidgets.QPushButton('')
        self.cancelButton = QtWidgets.QPushButton('')
        self.okButton.clicked.connect(self.accept)
        self.cancelButton.clicked.connect(self.reject)
        buttonLayout = QtWidgets.QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(self.okButton)
        buttonLayout.addWidget(self.cancelButton)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(self.languagesComboBox)
        mainLayout.addLayout(buttonLayout)
        self.setLayout(mainLayout)

        self.translateUi()

    def translateUi(self):
        self.translator.load(self.actualLang[2])
        ok = self.translator.translate('main', '&OK')
        if ok == '':
            ok = '&OK'
        self.okButton.setText(ok)
        cancel = self.translator.translate('main', 'C&ancel')
        if cancel == '':
            cancel = 'C&ancel'
        self.cancelButton.setText(cancel)
        self.setWindowTitle('I18N')

    def languageChanged(self):
        data = self.languagesComboBox.itemData(
            self.languagesComboBox.currentIndex(), QtCore.Qt.UserRole)
        self.actualLang = [
            data[0], 
            self.languagesComboBox.currentText(), 
            data[1]]
        self.translateUi()


