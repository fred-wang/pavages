# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------


"""
DESCRIPTION :
    Contient l'interface graphique.
"""


# importation des modules utiles :
import sys
import json

# importation des modules perso :
import utils, utils_functions, utils_webengine, utils_filesdirs, utils_exports
import pave, views, utils_dialogs

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui



class MainWindow(QtWidgets.QMainWindow):
    """
    La fenêtre principale.
    """
    
    def __init__(self, locale, translator, fileName):
        """
        mise en place de l'interface
        plus des variables utiles
        """
        QtWidgets.QMainWindow.__init__(self)

        # chemin utiles :
        self.beginDir = QtCore.QDir.currentPath()
        self.filesDir = QtCore.QDir.homePath()
        self.tempPath = utils_filesdirs.createTempAppDir(utils.PROGNAME + '-temp')
        # le nom du fichier :
        self.fileName = fileName
        self.mustSave = False

        # i18n :
        self.translator = translator
        self.locale = locale
        if utils.MODEBAVARD:
            print('locale:', locale)
        self.translations = {}
        trans_dir = QtCore.QDir('./translations')
        qmFiles = trans_dir.entryList(
            ('*.qm', ), 
            QtCore.QDir.Files, 
            QtCore.QDir.Name)
        defaultLanguage = False
        for qmFile in qmFiles:
            qmFile = './translations/{0}'.format(qmFile)
            translator = QtCore.QTranslator() 
            translator.load(qmFile)
            languageName = translator.translate(
                'main', 'LanguageName')
            if languageName == '':
                if not(defaultLanguage):
                    languageName = 'English'
                    defaultLanguage = True
            try:
                lang = qmFile.split('_')[1].split('.')[0]
            except:
                lang = 'en'
            self.translations[lang] = (qmFile, languageName)

        # on lit le fichier de config :
        self.configDict = {}
        self.configDir, first = utils_filesdirs.createConfigAppDir(utils.PROGNAME)
        configFileName = utils_functions.u(
            '{0}/config.json').format(self.configDir.canonicalPath())
        try:
            if QtCore.QFile(configFileName).exists():
                configFile = open(
                    configFileName, newline='', encoding='utf-8')
                self.configDict = json.load(configFile)
                configFile.close()
        except:
            self.configDict = {}
        # récupération ou initialisation des valeurs :
        for key in utils.DEFAULTCONFIG:
            if not(key in self.configDict):
                self.configDict[key] = utils.DEFAULTCONFIG[key]
        # on vérifie l'existence des fichiers :
        lastFiles = []
        for fileName in self.configDict['LASTFILES']:
            if QtCore.QFileInfo(fileName).isFile():
                lastFiles.append(fileName)
        self.configDict['LASTFILES'] = lastFiles

        # barre d'outils, actions et menu :
        self.toolBar = QtWidgets.QToolBar(self)
        #self.toolBar.setOrientation(QtCore.Qt.Vertical)
        self.toolBar.setIconSize(QtCore.QSize(32, 32))
        self.toolBar.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBar)
        self.createActionsAndMenus()
        self.updateLastFilesMenu()

        # la liste de 17 types de pavages :
        self.comboBox = QtWidgets.QComboBox()
        self.comboBox.setIconSize(QtCore.QSize(32,32))
        iconExt = 'png'
        iconExt = 'svg'
        for i in range(17):
            if i < 9:
                iconFile = 'images/type0{0}.{1}'.format(i + 1, iconExt)
            else:
                iconFile = 'images/type{0}.{1}'.format(i + 1, iconExt)
            label = ''
            self.comboBox.addItem(QtGui.QIcon(iconFile), label)
        self.comboBox.activated.connect(self.comboBoxChanged)
        self.comboBox.setCurrentIndex(1)
        self.comboBox.setIconSize(QtCore.QSize(32, 32))

        # Les 2 vues (pavé et pavage) :
        self.monPave = pave.Pave()
        self.gvPave = views.GraphicsViewPave(self)
        self.gvPavage = views.GraphicsViewPavage(self)
        self.gvPavage.setMinimumHeight(200)

        # L'aide contextuelle :
        self.helpWebView = utils_webengine.MyWebEngineView(self)
        self.helpFileName = ''

        # messages (croisements, etc) :
        self.messageLabel = QtWidgets.QLabel('')
        self.messageLabel.setAlignment(
            QtCore.Qt.AlignVCenter | QtCore.Qt.AlignCenter)
        #self.messageLabel.setMargin(10)

        # comboBox, pavé et messageLabel à gauche :
        leftVBoxLayout = QtWidgets.QVBoxLayout()
        leftVBoxLayout.addWidget(self.comboBox)
        leftVBoxLayout.addWidget(self.gvPave)
        leftVBoxLayout.addWidget(self.messageLabel)
        leftVGroupBox = QtWidgets.QGroupBox()
        leftVGroupBox.setLayout(leftVBoxLayout)
        leftVGroupBox.setFlat(True)
        # le helpWebView dans un QGroupBox :        
        helpContainer = QtWidgets.QAbstractScrollArea()
        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addWidget(self.helpWebView)
        helpContainer.setLayout(vBoxLayout)
        # pavage et helpWebView à droite :
        rightVSplitter = QtWidgets.QSplitter(self)
        rightVSplitter.setOrientation(QtCore.Qt.Vertical)
        rightVSplitter.addWidget(self.gvPavage)
        rightVSplitter.addWidget(helpContainer)
        rightVBoxLayout = QtWidgets.QVBoxLayout()
        rightVBoxLayout.addWidget(rightVSplitter)
        rightVGroupBox = QtWidgets.QGroupBox()
        rightVGroupBox.setLayout(rightVBoxLayout)
        rightVGroupBox.setFlat(True)
        rightVGroupBox.resize(500, 200)
        # Splitter horizontal :
        hSplitter = QtWidgets.QSplitter(self)
        hSplitter.setOrientation(QtCore.Qt.Horizontal)
        hSplitter.addWidget(leftVGroupBox)
        hSplitter.addWidget(rightVGroupBox)

        # le stackedWidget est le widget central :
        self.stackedWidget = QtWidgets.QStackedWidget()
        self.stackedWidget.addWidget(hSplitter)
        self.setCentralWidget(self.stackedWidget)
        self.closing = False

        # Le reste (on récupère le fichier passé en argument) :
        fileName = self.fileName
        self.updateTitle()
        self.comboBoxChanged()
        self.monPave.brush = QtGui.QPixmap('./images/0.png')
        self.translateUi()

        # ouverture d'un fichier passé en argument :
        self.fileName = fileName
        if self.fileName != '':
            self.fileOpen(self.fileName)

    def createActionsAndMenus(self):
        """
        création des différentes actions.
        Remplissage du menu et du toolbar.
        """
        # LES ACTIONS :
        # actions Fichier :
        self.actionOpen = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/fileopen.png'),
            triggered=self.fileOpen)
        self.actionSave = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/filesave.png'),
            triggered=self.fileSave)
        self.actionSave_as = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/filesaveas.png'),
            triggered=self.fileSaveAs)
        self.actionChooseLanguage = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/i18n.png'),
            triggered=self.chooseLanguage)
        self.actionCreateDesktopFileLinux = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/logo_linux.png'),
            triggered=self.ceateDesktopFileLinux)
        self.actionQuit = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/exit.png'),
            triggered=self.close)
        # actions Édition :
        self.actionBrush = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/0.png'),
            checkable=True,
            checked=False,
            triggered=self.doWithBrush)
        # actions Export :
        self.actionExportTilePNG = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/tile2png.png'),
            triggered=self.file_exportTilePNG)
        self.actionExportTileSVG = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/tile2svg.png'),
            triggered=self.file_exportTileSVG)
        self.actionExportSetPNG = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/set2png.png'),
            triggered=self.file_exportSetPNG)
        self.actionExportSetSVG = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/set2svg.png'),
            triggered=self.file_exportSetSVG)
        self.actionExportTilingPNG = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/tiling2png.png'),
            triggered=self.file_exportTilingPNG)
        self.actionExportTilingSVG = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/tiling2svg.png'),
            triggered=self.file_exportTilingSVG)
        # actions Aide :
        self.actionHelpPage = QtWidgets.QAction(
            self,
            shortcut=QtWidgets.QApplication.translate(
                'main', 'F1'),
            icon=QtGui.QIcon('images/help.png'),
            triggered=self.helpPage)
        self.actionAbout = QtWidgets.QAction(
            self, 
            icon=QtGui.QIcon('images/info.png'),
            triggered=self.about)
        # LE MENU :
        self.menuBar = QtWidgets.QMenuBar(self)
        self.setMenuBar(self.menuBar)
        # menu Fichier :
        self.fileMenu = QtWidgets.QMenu(self.menuBar)
        self.menuBar.addAction(self.fileMenu.menuAction())
        self.fileMenu.addAction(self.actionOpen)
        self.lastFilesMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate(
                'main', '&Recent Files'), 
            self)
        self.lastFilesMenu.setIcon(
            QtGui.QIcon('images/document-open-recent.png'))
        self.fileMenu.addMenu(self.lastFilesMenu)
        self.fileMenu.addAction(self.actionSave)
        self.fileMenu.addAction(self.actionSave_as)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.actionChooseLanguage)
        if utils.OS_NAME[0] == 'linux':
            self.fileMenu.addAction(
                self.actionCreateDesktopFileLinux)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.actionQuit)
        # menu Édition :
        self.editMenu = QtWidgets.QMenu(self.menuBar)
        self.menuBar.addAction(self.editMenu.menuAction())
        # menu Export :
        self.exportMenu = QtWidgets.QMenu(self.menuBar)
        self.menuBar.addAction(self.exportMenu.menuAction())
        self.exportMenu.addAction(self.actionExportTilePNG)
        self.exportMenu.addAction(self.actionExportTileSVG)
        self.exportMenu.addSeparator()
        self.exportMenu.addAction(self.actionExportSetPNG)
        self.exportMenu.addAction(self.actionExportSetSVG)
        self.exportMenu.addSeparator()
        self.exportMenu.addAction(self.actionExportTilingPNG)
        self.exportMenu.addAction(self.actionExportTilingSVG)
        # menu Aide :
        self.helpMenu = QtWidgets.QMenu(self.menuBar)
        self.menuBar.addAction(self.helpMenu.menuAction())
        self.helpMenu.addAction(self.actionHelpPage)
        self.helpMenu.addAction(self.actionAbout)
        # LES TOOLBARS :
        self.toolBar.addAction(self.actionQuit)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionOpen)
        self.toolBar.addAction(self.actionSave)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionBrush)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionExportTilingPNG)
        self.toolBar.addAction(self.actionExportTilingSVG)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionAbout)
        self.toolBar.addAction(self.actionHelpPage)

    def closeEvent(self, event):
        """
        on quitte.
        suppression du dossier temporaire
        """
        self.closing = True
        if self.testMustSave() == QtWidgets.QMessageBox.Cancel:
            event.ignore()
            return
        # on écrit le fichier de config :
        configFileName = utils_functions.u(
            '{0}/config.json').format(self.configDir.canonicalPath())
        configFile = open(
            configFileName, 'w', encoding='utf-8')
        # on ne garde que 10 fichiers :
        self.configDict['LASTFILES'] = self.configDict['LASTFILES'][:10]
        json.dump(self.configDict, configFile, indent=4)
        configFile.close()
        # suppression du dossier temporaire :
        utils_filesdirs.emptyDir(self.tempPath)
        QtCore.QDir.temp().rmdir(utils.PROGNAME)
        event.accept()

    def updateLastFilesMenu(self):
        """
        mise à jour du menu des fichiers récents
        """
        self.lastFilesMenu.clear()
        for fileName in self.configDict['LASTFILES']:
            newAction = QtWidgets.QAction(fileName, self)
            newAction.triggered.connect(self.fileOpen)
            newAction.setData(fileName)
            self.lastFilesMenu.addAction(newAction)

    def updateTitle(self, mustSave=False):
        """
        mise à jour du titre de la fenêtre.
        Suivant que le fichier est enregistré, a été modifié, ...
        """
        self.mustSave = mustSave
        if mustSave:
            mustSaveText = '*'
        else:
            mustSaveText = ''
        if self.fileName == '':
            fileNameText = QtWidgets.QApplication.translate(
                'main', 'No name')
        else:
            fileNameText = self.fileName
        title = utils_functions.u('{0} {1}  {2}[{3}]').format(
            utils.PROGNAME, utils.PROGVERSION, mustSaveText, fileNameText)
        self.setWindowTitle(title)

    def translateUi(self):
        """
        applique la langue sélectionnée à l'interface.
        Lancé à la fin de l'initialisation
        ou si on a choisi une autre langue.
        """
        # noms des barres d'outils :
        self.toolBar.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'toolBarFile'))
        # Fichier :
        self.fileMenu.setTitle(
            QtWidgets.QApplication.translate('main', '&File'))
        self.actionOpen.setText(
            QtWidgets.QApplication.translate('main', '&Open'))
        self.actionOpen.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Open a File'))
        self.actionSave.setText(
            QtWidgets.QApplication.translate('main', '&Save'))
        self.actionSave.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Save the File'))
        self.actionSave_as.setText(
            QtWidgets.QApplication.translate('main', 'Save &as'))
        self.actionSave_as.setStatusTip(
            QtWidgets.QApplication.translate(
                'main', 'Save the File with a different name'))
        self.actionChooseLanguage.setText(
            QtWidgets.QApplication.translate('main', '&Language'))
        self.actionChooseLanguage.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Choose Language'))
        self.actionCreateDesktopFileLinux.setText(
            QtWidgets.QApplication.translate('main', 'Create a launcher'))
        self.actionCreateDesktopFileLinux.setStatusTip(
            QtWidgets.QApplication.translate(
                'main', 
                'To create a .desktop file launcher in the folder of your choice'))
        self.actionQuit.setText(
            QtWidgets.QApplication.translate('main', '&Quit'))
        self.actionQuit.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Quit Application'))
        # Édition :
        self.editMenu.setTitle(
            QtWidgets.QApplication.translate('main', 'E&dit'))
        self.actionBrush.setText(
            QtWidgets.QApplication.translate('main', '&Brush'))
        self.actionBrush.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Show the Brush'))
        # Export :
        self.exportMenu.setTitle(
            QtWidgets.QApplication.translate('main', 'Expor&t'))
        self.pngTitle = QtWidgets.QApplication.translate(
            'main', 'Export as PNG File')
        self.pngExt = QtWidgets.QApplication.translate(
            'main', 'PNG Files (*.png)')
        self.svgTitle = QtWidgets.QApplication.translate(
            'main', 'Export as SVG File')
        self.svgExt = QtWidgets.QApplication.translate(
            'main', 'SVG Files (*.svg)')
        self.exportFileEndTile = QtWidgets.QApplication.translate(
            'main', 'TILE')
        self.exportFileEndFundamentalSet = QtWidgets.QApplication.translate(
            'main', 'SET')
        self.exportFileEndTiling = QtWidgets.QApplication.translate(
            'main', 'TILING')
        self.actionExportTilePNG.setText(
            QtWidgets.QApplication.translate('main', 'Export Tile as PNG'))
        self.actionExportTilePNG.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Export the Tile as PNG File'))
        self.actionExportTileSVG.setText(
            QtWidgets.QApplication.translate('main', 'Export Tile as SVG'))
        self.actionExportTileSVG.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Export the Tile as SVG File'))
        self.actionExportSetPNG.setText(
            QtWidgets.QApplication.translate('main', 'Export Fundamental Set as PNG'))
        self.actionExportSetPNG.setStatusTip(
            QtWidgets.QApplication.translate(
                'main', 'Export the Fundamental Set of Tiles as PNG File'))
        self.actionExportSetSVG.setText(
            QtWidgets.QApplication.translate('main', 'Export Fundamental Set as SVG'))
        self.actionExportSetSVG.setStatusTip(
            QtWidgets.QApplication.translate(
                'main', 'Export the Fundamental Set of Tiles as SVG File'))
        self.actionExportTilingPNG.setText(
            QtWidgets.QApplication.translate('main', 'Export Tiling as PNG'))
        self.actionExportTilingPNG.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Export the Tiling as PNG File'))
        self.actionExportTilingSVG.setText(
            QtWidgets.QApplication.translate('main', 'Export Tiling as SVG'))
        self.actionExportTilingSVG.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Export the Tiling as SVG File'))
        # Aide :
        self.helpMenu.setTitle(
            QtWidgets.QApplication.translate('main', '&Help'))
        self.actionHelpPage.setText(
            QtWidgets.QApplication.translate('main', '&HelpPage'))
        self.actionHelpPage.setStatusTip(
            QtWidgets.QApplication.translate('main', 'Show the Help in your browser'))
        self.actionAbout.setText(
            QtWidgets.QApplication.translate('main', '&About'))
        self.actionAbout.setStatusTip(
            QtWidgets.QApplication.translate('main', 'About this Application'))

        # les noms des 17 types de pavages :
        pavagesTitles = [
            (QtWidgets.QApplication.translate('main', 'p1'), 
             QtWidgets.QApplication.translate('main', 'asymmetric parallelogramic')),
            (QtWidgets.QApplication.translate('main', 'p2'), 
             QtWidgets.QApplication.translate('main', 'symmetric parallelogramic')),
            (QtWidgets.QApplication.translate('main', 'p3'), 
             QtWidgets.QApplication.translate('main', 'hexagonal 3-rotative')),
            (QtWidgets.QApplication.translate('main', 'p4'), 
             QtWidgets.QApplication.translate('main', 'square 4-rotative')),
            (QtWidgets.QApplication.translate('main', 'p6'), 
             QtWidgets.QApplication.translate('main', 'hexagonal 6-rotative')),
            (QtWidgets.QApplication.translate('main', 'pg'), 
             QtWidgets.QApplication.translate('main', 'gliding rectangular')),
            (QtWidgets.QApplication.translate('main', 'pgg'), 
             QtWidgets.QApplication.translate('main', 'bi-gliding rectangular')),
            (QtWidgets.QApplication.translate('main', 'pm'), 
             QtWidgets.QApplication.translate('main', 'mono-symmetric rectangular')),
            (QtWidgets.QApplication.translate('main', 'cm'), 
             QtWidgets.QApplication.translate('main', 'mono-symmetric rhombic')),
            (QtWidgets.QApplication.translate('main', 'pmg'), 
             QtWidgets.QApplication.translate('main', 'symmetric gliding rectangular')),
            (QtWidgets.QApplication.translate('main', 'pmm'), 
             QtWidgets.QApplication.translate('main', 'bi-symmetric rectangular')),
            (QtWidgets.QApplication.translate('main', 'cmm'), 
             QtWidgets.QApplication.translate('main', 'bi-symmetric rhombic')),
            (QtWidgets.QApplication.translate('main', 'p4g'), 
             QtWidgets.QApplication.translate('main', 'gliding 4-rotative square')),
            (QtWidgets.QApplication.translate('main', 'p3m1'), 
             QtWidgets.QApplication.translate('main', 'tri-symmetric hexagonal')),
            (QtWidgets.QApplication.translate('main', 'p31m'), 
             QtWidgets.QApplication.translate('main', 'symmetric 3-rotative hexagonal')),
            (QtWidgets.QApplication.translate('main', 'p4m'), 
             QtWidgets.QApplication.translate('main', 'totally symmetric square')),
            (QtWidgets.QApplication.translate('main', 'p6m'), 
             QtWidgets.QApplication.translate('main', 'totally symmetric hexagonal'))
            ]
        # mise à jour du combobox :
        for i in range(17):
            text = QtWidgets.QApplication.translate(
                'main', 'Type {0}: {1} [{2}]')
            label = utils_functions.u(text).format(
                i + 1, pavagesTitles[i][0], pavagesTitles[i][1])
            self.comboBox.setItemText(i, label)

        # autres :
        self.updateTitle(self.mustSave)
        self.statusBar().showMessage(
            QtWidgets.QApplication.translate(
                'main', 
                'A context menu is available by right-clicking'))
        self.gvPave.translateUi()
        mdFile = utils_functions.doLocale(
            self.locale, 'translations/HELP', '.md')
        self.helpFileName = utils_filesdirs.md2html(self, mdFile)
        self.scrollToAnchor()

    def chooseLanguage(self):
        """
        pour changer la langue à l'execution
        """
        dialog = utils_dialogs.LanguageChooserDlg(self)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        self.locale = dialog.actualLang[0]
        self.translator.load(dialog.actualLang[2])
        self.translateUi()

    def helpPage(self):
        """
        ouvre la page d'aide dans le navigateur
        """
        import utils_about
        utils_about.webHelpInBrowser(utils.HELPPAGE)

    def ceateDesktopFileLinux(self):
        """
        crée un lanceur (fichier .desktop).
        """
        title = QtWidgets.QApplication.translate(
            'main', 
            'Choose the Directory where the desktop file will be created')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self,
            title,
            QtCore.QDir.homePath(),
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            import utils_filesdirs
            result = utils_filesdirs.createDesktopFile(
                self, directory, utils.PROGNAME)

    def scrollToAnchor(self, anchor=''):
        """
        pour que le helpWebView se positionne
        sur l'aide du type de pavage sélectionné
        """
        if anchor == '':
            anchor = 'type{0}'.format(self.monPave.typePavage)
        url = QtCore.QUrl().fromLocalFile(self.helpFileName)
        url.setFragment(anchor)
        self.helpWebView.load(url)

    def about(self):
        """
        affiche la fenêtre À propos
        """
        import utils_about
        dialog = utils_about.AboutDlg(
            self, self.locale, icon='./images/icon.svg')
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)

    def disableInterface(self, dialog):
        """
        cache les barres d'outils lors de l'appel d'un dialog
        affiché dans le stackedWidget.
        On retourne aussi :
            lastIndex : le widget affiché précédemment dans le stackedWidget
            lastTitle : le titre de la fenêtre
            lastMessage : le texte du statusBar
        """
        lastIndex = self.stackedWidget.currentIndex()
        lastTitle = self.windowTitle()
        lastMessage = self.statusBar().currentMessage()
        for widget in (self.menuBar, self.toolBar):
            widget.setVisible(False)
        self.setWindowTitle(dialog.windowTitle())
        self.statusBar().clearMessage()
        newIndex = self.stackedWidget.addWidget(dialog)
        self.stackedWidget.setCurrentIndex(newIndex)
        return (lastIndex, lastTitle, lastMessage)

    def enableInterface(self, dialog, lastState=(0, '', '')):
        """
        replace les barres d'outils après l'appel d'un dialog
        affiché dans le stackedWidget.
        On remet aussi l'état précédent via lastState :
            widget affiché dans le stackedWidget
            titre de la fenêtre
            texte du statusBar
        """
        if self.closing:
            return
        self.stackedWidget.removeWidget(dialog)
        self.stackedWidget.setCurrentIndex(lastState[0])
        self.setWindowTitle(lastState[1])
        self.statusBar().showMessage(lastState[2])
        for widget in (self.menuBar, self.toolBar):
            widget.setVisible(True)

    def setPenProperties(self):
        """
        ouvre le dialogue de config du contour.
        """
        dialog = utils_dialogs.PenPropertiesDlg(self)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        self.monPave.lineWidth = dialog.pen.width()
        self.monPave.lineStyle = dialog.pen.style()
        self.monPave.lineJoin = dialog.pen.joinStyle()
        self.updateViews(paveModifie=True)

    def setPavageConfig(self):
        """
        ouvre le dialogue de config du pavage.
        """
        dialog = utils_dialogs.PavageConfigDlg(self)
        dialog.nbXSpinBox.setValue(
            self.gvPavage.GraphicsItemPavage.nbX)
        dialog.nbYSpinBox.setValue(
            self.gvPavage.GraphicsItemPavage.nbY)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        self.gvPavage.GraphicsItemPavage.nbX = dialog.nbXSpinBox.value()
        self.gvPavage.GraphicsItemPavage.nbY = dialog.nbYSpinBox.value()
        self.updateViews()

    def comboBoxChanged(self):
        """
        on charge un autre type de pavage.
        On teste s'il y a eu des modifications.
        """
        if self.testMustSave() == QtWidgets.QMessageBox.Cancel:
            self.comboBox.setCurrentIndex(
                self.monPave.typePavage - 1)
            return
        self.monPave.reInit(self.comboBox.currentIndex() + 1)
        self.reset_Brush()
        self.updateViews()
        self.monPave.calculer()
        self.gvPave.createnodes()
        self.fileName = ''
        self.updateTitle()
        self.scrollToAnchor()

    def updateViews(
            self, 
            paveModifie=False, 
            affichagePave=True, 
            affichagePavage=True, 
            paveCalculer=False):
        """
        mise à jour de l'affichage du pavé, du pavage, etc.
        selon les paramètres passés, 
        la mise à jour est plus ou moins importante.
        """
        if paveCalculer:
            self.monPave.calculer()
        if paveModifie:
            self.updateTitle(mustSave=True)
        if affichagePave:
            self.gvPave.update()
            self.gvPave.scene().update()
        if affichagePavage:
            self.gvPavage.scene().update()

    def doWithBrush(self):
        """
        le pavage utilise ou non une brosse
        """
        withBrush = self.actionBrush.isChecked()
        self.monPave.withBrush = withBrush
        self.gvPave.withBrush = withBrush
        self.gvPavage.withBrush = withBrush
        self.updateViews(paveModifie=True)

    def selectBrush(self):
        """
        sélectionne le fichier PNG servant de brosse.
        """
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self,
            QtWidgets.QApplication.translate('main', 'Open'),
            self.filesDir,
            QtWidgets.QApplication.translate('main', 'PNG Files (*.png)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.filesDir = QtCore.QFileInfo(fileName).absolutePath()
        self.monPave.brush = QtGui.QPixmap(fileName)
        self.monPave.brushFileName = fileName
        if not(self.monPave.withBrush):
            self.actionBrush.setChecked(True)
            self.doWithBrush()
        self.updateViews(paveModifie=True)

    def reset_Brush(self):
        """
        remet le brush par défaut.
        En cas de changement de pavage.
        """
        self.monPave.brush = QtGui.QPixmap('./images/0.png')
        self.actionBrush.setChecked(False)
        self.doWithBrush()

    def testMustSave(self):
        """
        demande s'il faut enregistrer les modifications.
        """
        if self.mustSave:
            message = QtWidgets.QApplication.translate(
                'main', 'File must be saved ?')
            reply = QtWidgets.QMessageBox.question(
                self, 
                QtWidgets.QApplication.translate('main', 'Save ?'),
                message, 
                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No | QtWidgets.QMessageBox.Cancel)
            if reply == QtWidgets.QMessageBox.Yes:
                if self.fileName == '':
                    self.fileSaveAs()
                elif self.saveFilePav(self.fileName):
                    self.mustSave = False
            return reply

    def fileOpen(self, fileName=''):
        """
        ouverture d'un fichier *.pav
        Elle peut être appelée par l'action "ouvrir",
        mais aussi en cliquant sur un fichier récent,
        ou enfin par le dernier paramètre passé au lancement.
        La variable "who" permet de récupérer qui a demandé
        l'ouverture de fichier.
        """
        if self.testMustSave() == QtWidgets.QMessageBox.Cancel:
            return
        if self.sender() == None:
            # on vérifie l'existence du fichier car il peut y avoir
            # d'autres paramètres dans la ligne de commande :
            who = 'param'
            if not(QtCore.QFileInfo(fileName).isFile()):
                return
        elif self.sender() != self.actionOpen:
            # on a demandé un fichier récent :
            who = 'recent'
            fileName = self.sender().data()
        else:
            # on commence par sélectionner le fichier :
            who = 'action'
            fileName = QtWidgets.QFileDialog.getOpenFileName(
                self,
                QtWidgets.QApplication.translate(
                    'main', 'Open'),
                self.filesDir,
                QtWidgets.QApplication.translate(
                    'main', 'Pavage Files (*.pav)'))
            fileName = utils_functions.verifyLibs_fileName(fileName)
            if fileName == '':
                return
        self.filesDir = QtCore.QFileInfo(fileName).absolutePath()
        self.openFilePav(fileName)
        self.fileName = fileName
        self.updateViews()
        self.updateTitle()
        self.scrollToAnchor()

    def fileSave(self):
        """
        enregistrement d'un fichier *.pav
        """
        if self.fileName == '':
            self.fileSaveAs()
        elif self.saveFilePav(self.fileName):
            self.updateTitle()

    def fileSaveAs(self):
        """
        enregistrer sous un fichier *.pav
        """
        proposedName = QtWidgets.QApplication.translate(
            'main', 'No name')
        proposedName = utils_functions.u('{0}/{1}.pav').format(
            self.filesDir, proposedName)
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self,
            QtWidgets.QApplication.translate(
                'main', 'Save as'),
            proposedName,
            QtWidgets.QApplication.translate(
                'main', 'Pavage Files (*.pav)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.filesDir = QtCore.QFileInfo(fileName).absolutePath()
        if self.saveFilePav(fileName):
            self.fileName = fileName
            self.updateTitle()

    def openFilePav(self, fileName):
        result = utils_exports.openFilePav(self, fileName)
        # on met à jour les fichiers récents :
        if (fileName in self.configDict['LASTFILES']):
            self.configDict['LASTFILES'].remove(fileName)
        if result:
            self.configDict['LASTFILES'].insert(0, fileName)
        self.updateLastFilesMenu()

    def saveFilePav(self, fileName):
        if utils_exports.saveFilePav(self, fileName):
            # on met à jour les fichiers récents :
            if (fileName in self.configDict['LASTFILES']):
                self.configDict['LASTFILES'].remove(fileName)
            self.configDict['LASTFILES'].insert(0, fileName)
            self.updateLastFilesMenu()
            return True
        return False

    def file_exportTilePNG(self):
        utils_exports.exportPNG(self, what='TILE')

    def file_exportTileSVG(self):
        utils_exports.exportSVG(self, what='TILE')

    def file_exportSetPNG(self):
        utils_exports.exportPNG(self, what='SET')

    def file_exportSetSVG(self):
        utils_exports.exportSVG(self, what='SET')

    def file_exportTilingPNG(self):
        utils_exports.exportPNG(self, what='TILING')

    def file_exportTilingSVG(self):
        utils_exports.exportSVG(self, what='TILING')


