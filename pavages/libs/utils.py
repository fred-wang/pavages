# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient les variables et fonctions utiles au programme.
"""

# importation des modules utiles :
import sys



"""
****************************************************
    VERSIONS DE PYTHON, QT, ETC
****************************************************
"""

# version de Python :
PYTHONVERSION = sys.version_info[0] * 10 + sys.version_info[1]

# PyQt5 :
PYQT = ''
try:
    # on teste d'abord PyQt5 :
    from PyQt5 import QtCore, QtWidgets, QtGui
    PYQT = 'PYQT5'
except:
    pass
if PYQT == '':
    print('YOU MUST INSTALL PYQT !')

# version de Qt :
qtVersion = QtCore.qVersion()

# détection du système (nom et 32 ou 64) :
OS_NAME = ['', '']
def detectPlatform():
    global OS_NAME
    # 32 ou 64 bits :
    if sys.maxsize > 2**32:
        bits = 64
    else:
        bits = 32
    # platform et osName :
    platform = sys.platform
    osName = ''
    if platform.startswith('linux'):
        osName = 'linux'
    elif platform.startswith('win'):
        osName = 'win'
    elif platform.startswith('freebsd'):
        osName = 'freebsd'
    elif platform.startswith('darwin'):
        import platform
        if 'powerpc' in platform.uname():
            osName = 'powerpc'
        else:
            osName = 'mac'
    OS_NAME = [osName, bits]
detectPlatform()

MODEBAVARD = False
if 'MODEBAVARD' in sys.argv:
    MODEBAVARD = True
print('MODEBAVARD: ', MODEBAVARD)



"""
****************************************************
    VARIABLES LIÉES AU LOGICIEL
****************************************************
"""

PROGNAME = 'Pavages'
PROGLINK = 'pavages'
PROGVERSION = '2___2021-02'
HELPPAGE = 'http://pascal.peter.free.fr/pavages.html'



"""
****************************************************
    DIVERS
****************************************************
"""

SCENE_COEFF = 160
SCENE_BEGIN = -200
SCENE_SIZE = 400

def internal2view(P):
    return QtCore.QPointF(SCENE_COEFF * P.X, - SCENE_COEFF * P.Y)

def view2internal(P):
    return (P.x() / SCENE_COEFF, - P.y() / SCENE_COEFF)

DEFAULTCONFIG = {
    'LASTFILES': [], 
    }

iconExt = 'png'
#iconExt = 'svgz'


