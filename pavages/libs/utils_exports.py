# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient les fonctions pour 
    ouvrir et enregistrer les pavages, 
    les exporter en PNG, SVG.
"""


# importation des modules utiles :
import json

# importation des modules perso :
import utils, utils_functions, pave

# PyQt5 :
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5 import QtSvg



"""
****************************************************
    OUVRIR ET ENREGISTRER
****************************************************
"""

def openFilePav(main, fileName):
    """
    les fichiers sont en fait au format json.
    On récupère un dictionnaire correspondant au fichier,
    puis on crée le pavage en fonction du contenu.
    """
    result = False
    utils_functions.doWaitCursor()
    try:
        fileDict = {}
        import json
        inFile = open(
            fileName, newline='', encoding='utf-8')
        fileDict = json.load(inFile)
        inFile.close()

        # initialisation suivant le type :
        i = fileDict['DATA']['type']
        main.comboBox.setCurrentIndex(i - 1)
        main.monPave.reInit(i)
        main.monPave.calculer()

        # les points de contrôle (repère + selon le type du pavage) :
        for i in range(3):
            [X, Y] = fileDict['DATA']['controlPoints'][str(i)]
            main.monPave.controlPoints[i].X = X
            main.monPave.controlPoints[i].Y = Y
        k = 2
        if main.monPave.nbPointsSupp > 0:
            for i in range(main.monPave.nbPointsSupp):
                k = k + 1
                [X, Y] = fileDict['DATA']['controlPoints'][str(k)]
                main.monPave.controlPoints[k].X = X
                main.monPave.controlPoints[k].Y = Y

        # les points supplémentaires sur chaque arc :
        for i in range(main.monPave.nbArcs):
            nbPoints = fileDict['DATA']['arcs'][str(i)]
            if nbPoints > 0:
                for j in range(nbPoints):
                    main.monPave.creePoint(i)
                for j in range(nbPoints):
                    k = k + 1
                    [X, Y] = fileDict['DATA']['controlPoints'][str(k)]
                    main.monPave.controlPoints[k].X = X
                    main.monPave.controlPoints[k].Y = Y

        # couleurs des pavés de l'ensemble fondamental :
        for i in range(main.monPave.nbPaves):
            color = fileDict['DATA']['colors'][str(i)]
            main.monPave.colors[i] = color

        # description du contour :
        line = fileDict['DATA']['outline']['line']
        main.monPave.lineWidth = line['width']
        main.monPave.lineStyle = line['style']
        main.monPave.lineCap = line['cap']
        main.monPave.lineJoin = line['join']
        color = fileDict['DATA']['outline']['color']
        main.monPave.lineColor = color

        # la brosse utilisée :
        brush = fileDict['DATA']['brush']
        if brush['withBrush']:
            brushFileName = brush['brushFileName']
            if brushFileName == '':
                brushFileName = './images/0.png'
            else:
                brushFileName = utils_functions.u('{0}/{1}').format(
                    QtCore.QFileInfo(fileName).absoluteFilePath(), 
                    brush['relativeFileName'])
                brushFileName = QtCore.QFileInfo(brushFileName).absoluteFilePath()
                if not(QtCore.QFileInfo(brushFileName).exists()):
                    brushFileName = brush['longFileName']
                    if not(QtCore.QFileInfo(brushFileName).exists()):
                        brushFileName = './images/0.png'
            main.monPave.brush = QtGui.QPixmap(brushFileName)
            main.monPave.brushFileName = brushFileName
            main.monPave.withBrush = True
        else:
            main.monPave.withBrush = False
        main.actionBrush.setChecked(main.monPave.withBrush)
        main.doWithBrush()

        main.monPave.calculer()
        main.gvPave.createnodes()
        result = True
    finally:
        utils_functions.restoreCursor()
        if not(result):
            text = QtWidgets.QApplication.translate(
                'main', 'Cannot open file {0}.')
            text = label = utils_functions.u(text).format(
                fileName)
            QtWidgets.QMessageBox.warning(
                main, utils.PROGNAME, text)
        return result

def saveFilePav(main, fileName):
    """
    enregistrement du pavage en fichier *.pav.
    En fait c'est un fichier json.
    Donc on forme un dictionnaire avant de le sauvegarder.
    """
    result = False
    utils_functions.doWaitCursor()
    try:
        # remplissage du dictionnaire :
        fileDict = {}
        fileDict['PROG'] = {
            'name': utils.PROGNAME, 
            'version': utils.PROGVERSION, 
            }
        fileDict['DATA'] = {
            'type': main.monPave.typePavage, 
            'controlPoints': {}, 
            'arcs': {}, 
            'colors': {}, 
            'outline': {}, 
            'brush': {}, 
            }
        # les points de contrôle :
        for P in main.monPave.controlPoints:
            i = main.monPave.controlPoints.index(P)
            fileDict['DATA']['controlPoints'][str(i)] = [P.X, P.Y]
        # le nombre de points supplémentaires
        # sur chaque arc :
        for i in range(main.monPave.nbArcs):
            fileDict['DATA']['arcs'][str(i)] = main.monPave.f[i]
        # couleurs des pavés de l'ensemble fondamental :
        for i in range(main.monPave.nbPaves):
            fileDict['DATA']['colors'][str(i)] = main.monPave.colors[i]
        # description du contour :
        fileDict['DATA']['outline'] = {
            'line': {
                'width': main.monPave.lineWidth, 
                'style': main.monPave.lineStyle, 
                'cap': main.monPave.lineCap, 
                'join': main.monPave.lineJoin, 
                }, 
            'color': main.monPave.lineColor, 
            }
        # la brosse utilisée :
        fileDict['DATA']['brush'] = {
            'withBrush': main.monPave.withBrush, 
            }
        if main.monPave.withBrush:
            if main.monPave.brushFileName == './images/0.png':
                fileDict['DATA']['brush']['brushFileName'] = ''
            else:
                brushFileName = QtCore.QFileInfo(
                    main.monPave.brushFileName).fileName()
                longFileName = utils_functions.u(
                    main.monPave.brushFileName)
                relativeFileName = utils_functions.u(
                    QtCore.QDir(fileName).relativeFilePath(
                        main.monPave.brushFileName))
                fileDict['DATA']['brush']['brushFileName'] = brushFileName
                fileDict['DATA']['brush']['longFileName'] = longFileName
                fileDict['DATA']['brush']['relativeFileName'] = relativeFileName

        # sauvegarde en json :
        outFile = open(
            fileName, 'w', encoding='utf-8')
        json.dump(fileDict, outFile, indent=4)
        outFile.close()
        result = True
    finally:
        utils_functions.restoreCursor()
        if not(result):
            try:
                errorText = outFile.errorString()
            except:
                errorText = ''
            text = QtWidgets.QApplication.translate(
                'main', 'Cannot write file {0}:\n{1}.')
            text = label = utils_functions.u(text).format(
                fileName, errorText)
            QtWidgets.QMessageBox.warning(
                main, utils.PROGNAME, text)
        return result



"""
****************************************************
    EXPORTATIONS (PNG ET SVG)
****************************************************
"""

def selectFileName(main, title, extensionText, extension, what='TILE'):
    """
    permet de sélectionner le nom du fichier à exporter.
    Le nom proposé tient compte de ce qui est exporté
    (pavé, ensemble fondamental ou pavage)
    et du format d'export.
    """
    if what == 'TILE':
        what = main.exportFileEndTile
    elif what == 'SET':
        what = main.exportFileEndFundamentalSet
    elif what == 'TILING':
        what = main.exportFileEndTiling
    proposedName = utils_functions.u('{0}/{1}_{2}.{3}').format(
        main.filesDir, 
        QtCore.QFileInfo(main.fileName).baseName(), 
        what,
        extension)
    fileName = QtWidgets.QFileDialog.getSaveFileName(
        main, title, proposedName, extensionText)
    fileName = utils_functions.verifyLibs_fileName(fileName)
    return fileName

def exportPNG(main, what='TILE'):
    """
    exporte le pavé, l'ensemble fondamental
    ou le pavage en PNG.
    On utilise 2 fois le QPainter pour gérer correctement 
    la transparence (galère à trouver).
    Les dimensions sont élargies pour que l'épaisseur 
    du contour soit prise en compte.
    """
    fileName = selectFileName(main, main.pngTitle, main.pngExt, 'png', what)
    if fileName == '':
        return
    utils_functions.doWaitCursor()
    try:
        main.filesDir = QtCore.QFileInfo(fileName).absolutePath()
        # récupération des éléments (dimensions etc) :
        lineWidth = main.monPave.lineWidth
        if what == 'TILE':
            sommets = [utils.internal2view(point) for point in main.monPave.sommets]
            polygon = QtGui.QPolygonF(sommets)
            boundingRect = polygon.boundingRect().toRect()
            dimensions = [
                boundingRect.topLeft().x(), 
                boundingRect.topLeft().y(), 
                boundingRect.bottomRight().x(), 
                boundingRect.bottomRight().y()]
        elif what == 'SET':
            dimensions = main.gvPave.fundamentalSet.dimensions
        elif what == 'TILING':
            dimensions = main.gvPavage.GraphicsItemPavage.dimensions
        boundingRect = QtCore.QRect(
            dimensions[0] - 2 * lineWidth, 
            dimensions[1] - 2 * lineWidth, 
            dimensions[2] - dimensions[0] + 4 * lineWidth, 
            dimensions[3] - dimensions[1] + 4 * lineWidth, 
            )
        decalageX = - boundingRect.bottomLeft().x()
        decalageY = - boundingRect.topRight().y()
        # création du QPixmap et transparence :
        pixmap = QtGui.QPixmap(
            boundingRect.width(), 
            boundingRect.height())
        painter = QtGui.QPainter()
        painter.begin(pixmap)
        painter.translate(decalageX, decalageY)
        painter.eraseRect(boundingRect)
        painter.end()
        pixmap.setMask(pixmap.createHeuristicMask())
        # on ouvre à nouveau le QPainter pour tracer :
        painter.begin(pixmap)
        painter.translate(decalageX, decalageY)
        if what == 'TILE':
            main.gvPave.fundamentalSet.doPaint(
                painter, repere=False, onlyPave=True)
        elif what == 'SET':
            main.gvPave.fundamentalSet.doPaint(
                painter, repere=False)
        elif what == 'TILING':
            main.gvPavage.GraphicsItemPavage.doPaint(
                painter)
        painter.end()
        # sauvegarde du fichier :
        pixmap.save(fileName, 'PNG')
    finally:
        utils_functions.restoreCursor()

def exportSVG(main, what='TILE'):
    """
    exports en SVG.
    """
    fileName = selectFileName(main, main.svgTitle, main.svgExt, 'svg', what)
    if fileName == '':
        return
    utils_functions.doWaitCursor()
    try:
        main.filesDir = QtCore.QFileInfo(fileName).absolutePath()
        # récupération des éléments (dimensions etc) :
        lineWidth = main.monPave.lineWidth
        if what == 'TILE':
            sommets = [utils.internal2view(point) for point in main.monPave.sommets]
            polygon = QtGui.QPolygonF(sommets)
            boundingRect = polygon.boundingRect().toRect()
            dimensions = [
                boundingRect.topLeft().x(), 
                boundingRect.topLeft().y(), 
                boundingRect.bottomRight().x(), 
                boundingRect.bottomRight().y()]
        elif what == 'SET':
            dimensions = main.gvPave.fundamentalSet.dimensions
        elif what == 'TILING':
            dimensions = main.gvPavage.GraphicsItemPavage.dimensions
        boundingRect = QtCore.QRect(
            dimensions[0] - 2 * lineWidth, 
            dimensions[1] - 2 * lineWidth, 
            dimensions[2] - dimensions[0] + 4 * lineWidth, 
            dimensions[3] - dimensions[1] + 4 * lineWidth, 
            )
        decalageX = - boundingRect.bottomLeft().x()
        decalageY = - boundingRect.topRight().y()
        # création du QSvgGenerator :
        svgRenderer = QtSvg.QSvgGenerator()
        svgRenderer.setFileName(fileName)
        svgRenderer.setSize(boundingRect.size())
        svgRenderer.setResolution(1.25 * svgRenderer.resolution())
        # on ouvre un QPainter pour tracer :
        painter = QtGui.QPainter(svgRenderer)
        painter.translate(decalageX, decalageY)
        if what == 'TILE':
            main.gvPave.fundamentalSet.doPaint(
                painter, repere=False, onlyPave=True)
        elif what == 'SET':
            main.gvPave.fundamentalSet.doPaint(
                painter, repere=False)
        elif what == 'TILING':
            main.gvPavage.GraphicsItemPavage.doPaint(painter)
        painter.end()
    finally:
        utils_functions.restoreCursor()


