# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of Pavages project.
# Name:         Pavages
# Copyright:    (C) 1998-2021 Pascal Peter
# License:      GNU General Public License version 3
# Website:      http://pascal.peter.free.fr
# Email:        pascal.peter at free.fr
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    La classe Pave.
    Gère les propriétés du pavé initial et les calculs de ses attributs.
"""


# importation des modules utiles :
import random

# importation des modules perso :
import geom



MAGIC_NUMBER = 123456789

class Pave:

    def __init__(self, typePavage=1):
        self.brush = None
        self.brushFileName = ''
        self.withBrush = False
        self.reInit(typePavage)

    def reInit(self, typePavage=1):
        """
        initialisation des attributs du pavé
        en fonction de son type
        """
        self.typePavage = typePavage
        # Les éléments du Pavé :
        self.O = geom.Point(0, 0)
        self.I = geom.Point(1, 0, genre=geom.LIBRE)
        if typePavage in (3, 4, 5, 13, 14, 15, 16, 17):
            self.J = geom.Point(0, 1)
        elif typePavage in (6, 7, 8, 9, 10, 11, 12):
            self.J = geom.Point(0, 1, genre=geom.LIE)
        else:
            self.J = geom.Point(0, 1, genre=geom.LIBRE)
        self.controlPoints = [self.O, self.I, self.J]
        structures = {
            1: (0, 2, 1), 
            2: (1, 4, 2), 
            3: (1, 3, 3), 
            4: (1, 3, 4), 
            5: (1, 3, 6), 
            6: (2, 3, 2), 
            7: (2, 4, 4), 
            8: (1, 1, 2), 
            9: (1, 2, 2), 
            10: (1, 2, 4), 
            11: (0, 0, 4), 
            12: (0, 1, 4), 
            13: (0, 1, 8), 
            14: (0, 0, 6), 
            15: (0, 1, 6), 
            16: (0, 0, 8), 
            17: (0, 0, 12)}
        # nbPointsSupp : Nombre de points déplaçables 
        #   supplémentaires obligatoires
        # nbArcs : Nombre d'arcs du Pavé
        # nbPaves : Nombre de pavés dans l'ensemble fordamental
        self.nbPointsSupp = structures[self.typePavage][0]
        self.nbArcs = structures[self.typePavage][1]
        self.nbPaves = structures[self.typePavage][2]

        # nombre de points facutatifs sur chaque arc :
        self.f = []
        for i in range(self.nbArcs):
            self.f.append(0)
        # Nombre total de points facultatifs
        self.ff = 0
        # Nombre de points déplaçables.
        # d = 3 + m + ff
        self.d = 3 + self.nbPointsSupp + self.ff
        # Nombre de points du contour du pavé :
        self.s = 0
        # Liste des sommets du Pavé :
        self.sommets = []
        # liste des transformations pour
        # générer les autres pavés de l'ensemble
        # fondamental :
        self.transformations = []

        # Les attributs du Pavage :
        self.colors = []
        for i in range(12):
            color = []
            for i in range(4):
                color.append(random.uniform(127, 255))
            self.colors.append(color)
        self.lineWidth = 1
        self.lineStyle = 1
        self.lineCap = 0
        self.lineJoin = 0
        self.lineColor = [0, 0, 0, 255]
        if self.nbPointsSupp == 1:
            self.controlPoints.append(
                geom.Point(0.2, 0, genre=geom.LIE))
        elif self.nbPointsSupp == 2:
            self.controlPoints.append(
                geom.Point(0.2, 0, genre=geom.LIE))
            self.controlPoints.append(
                geom.Point(0.5, 0, genre=geom.LIE))
        self.brush = None
        self.brushFileName = ''
        self.withBrush = False

    def repereIsocele(self):
        """
        Oblige le repère OIJ à rester isocèle
        """
        vecteurOI = geom.vecteur(self.O, self.I)
        vecteurOJ = geom.vecteur(self.O, self.J)
        angle = geom.angleOriente(vecteurOI, vecteurOJ)
        P = geom.rotation(self.I, self.O, angle)
        self.J.X = P.X
        self.J.Y = P.Y

    def repereEquilateral(self):
        """
        Oblige le repère OIJ à rester équilatéral
        """
        P = geom.rotation(self.I, self.O, geom.PI / 3)
        self.J = P

    def repereRectangle(self):
        """
        Oblige le repère OIJ à rester rectangulaire
        """
        P = geom.rotation(self.I, self.O, geom.PI / 2)
        P = geom.projectionOrthogonale(self.J, self.O,P)
        self.J.X = P.X
        self.J.Y = P.Y

    def repereCarre(self):
        """
        Oblige le repère OIJ à rester carré
        """
        P = geom.rotation(self.I, self.O, geom.PI / 2)
        self.J = P

    def creePoint(self, arc):
        """
        Création du premier point facultatif sur l'arc demandé
        """
        j = 3 + self.nbPointsSupp
        for i in range(arc):
            j = j + self.f[i]
        self.f[arc] = self.f[arc] + 1
        self.controlPoints.insert(
            j, 
            geom.Point(
                MAGIC_NUMBER, 0, genre=geom.FACULTATIF, arc=arc))

    def ajoutePoint(self, P):
        """
        Création d'un nouveau point sur celui sélectionné
        """
        for CurPoint in self.controlPoints:
            if CurPoint == P:
                j = self.controlPoints.index(CurPoint)
        if P.genre == geom.FACULTATIF:
            self.f[P.arc] = self.f[P.arc] + 1
            self.controlPoints.insert(
                j, 
                geom.Point(
                    P.X, P.Y, genre=geom.FACULTATIF, arc=P.arc))

    def supprimePoint(self, P):
        """
        Suppression d'un point facultatif
        """
        if P.genre == geom.FACULTATIF:
            self.f[P.arc] = self.f[P.arc] - 1
            self.controlPoints.remove(P)

    def testCroisements(self):
        """
        teste si le pavé est croisé.
        Un polygone croisé ne peut pas faire
        un pavage.
        Par contre ça peut être joli...
        """
        nbSommets = len(self.sommets)
        for a in range(nbSommets - 3):
            b = a + 1
            for c in range(a + 2, nbSommets - 1):
                d = c + 1
                if geom.intersectionSegments(
                    self.sommets[a], 
                    self.sommets[b], 
                    self.sommets[c], 
                    self.sommets[d]):
                    return True
        return False

    def calculer(self):
        """
        la procédure la plus longue.
        Pour chaque type de pavage,
        on calcule le pavé et on remplit
        la liste des transformations permettant
        de générer l'ensemble fondamental.
        """
        self.ff = 0
        for i in range(self.nbArcs):
            self.ff = self.ff + self.f[i]
        self.d = 3 + self.nbPointsSupp + self.ff
        self.sommets = []
        self.transformations = []
        if self.typePavage == 1:
            vecteurOI = geom.vecteur(self.O, self.I)
            vecteurOJ = geom.vecteur(self.O, self.J)
            # Calcul des points du contour :
            self.s = (self.d - 1) * 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # Les 4 sommets du parallélogramme JOI? :
            k, j = 2, 0
            self.sommets[j] = self.controlPoints[k]
            k, j = 0, 1 + self.f[1]
            self.sommets[j] = self.controlPoints[k]
            k, j = 1, 2 + self.f[0] + self.f[1]
            self.sommets[j] = self.controlPoints[k]
            j = 3 + self.f[0] + 2 * self.f[1]
            self.sommets[j] = geom.translation(
                self.controlPoints[k], vecteurOJ)
            # [JO] et son translaté :
            for i in range(self.f[1]):
                k = 3 + self.f[0] + i
                j = 1 + i
                P = self.controlPoints[k]
                if P.X == MAGIC_NUMBER:
                    P.X, P.Y = self.J.X, self.J.Y
                self.sommets[j] = P
                j = 2 + self.f[0] + 2 * self.f[1] - i
                self.sommets[j] = geom.translation(P, vecteurOI)
            # [OI] et son translaté :
            for i in range(self.f[0]):
                k = 3 + i
                j = 2 + self.f[1] + i
                P = self.controlPoints[k]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.I.X, self.I.Y
                self.sommets[j] = P
                j = self.s - 1 - i
                self.sommets[j] = geom.translation(P, vecteurOJ)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
        elif self.typePavage == 2:
            # Calcul des Points Utiles (centres,...) :
            centre = []
            centre.append(self.O)
            centre.append(geom.milieu(self.O, self.I))
            centre.append(geom.milieu(self.I, self.J))
            centre.append(geom.milieu(self.O, self.J))
            # Calcul des points du contour :
            self.s = self.d * 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et les 3 autres centres de symétrie :
            self.sommets[0] = self.O
            j = 2 + self.f[0] +  self.f[1]
            self.sommets[j] = centre[1]
            j = 4 + self.f[0] + 2 * self.f[1] +  self.f[2]
            self.sommets[j] = centre[2]
            j = 6 + self.f[0] + 2 * self.f[1] + 2 * self.f[2] + self.f[3]
            self.sommets[j] = centre[3]
            # M et ses 3 symétriques successifs :
            P = self.controlPoints[3]
            j = 1 + self.f[0]
            self.sommets[j] = P
            j = 3 + self.f[0] + 2 * self.f[1]
            P = geom.symetrieCentrale(P, centre[1])
            self.sommets[j] = P
            j = 5 + self.f[0] + 2 * self.f[1] + 2 * self.f[2]
            P = geom.symetrieCentrale(P, centre[2])
            self.sommets[j] = P
            j = 7 + self.f[0] + 2 * self.f[1] + 2 * self.f[2] + 2 * self.f[3]
            self.sommets[j] = geom.symetrieCentrale(P, centre[3])
            # Les points déplaçables facultatifs et leurs symétriques :
            k = 4
            j = 1
            l = self.s - 1
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[0].X, centre[0].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.symetrieCentrale(P, centre[0])
            k = k + self.f[0]
            j = j + 1 + self.f[0]
            l = 2 + self.f[0] + 2 * self.f[1]
            for i in range(self.f[1]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[1].X, centre[1].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.symetrieCentrale(P, centre[1])
            k = k + self.f[1]
            j = j + 2 + 2 * self.f[1]
            l = l + 2 + 2 * self.f[2]
            for i in range(self.f[2]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[2].X, centre[2].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.symetrieCentrale(P, centre[2])
            k = k + self.f[2]
            j = j + 2 + 2 * self.f[2]
            l = l + 2 + 2 * self.f[3]
            for i in range(self.f[3]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[3].X, centre[3].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.symetrieCentrale(P, centre[3])
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['rotation', self.O, geom.PI])
            centre[2]
        elif self.typePavage == 3:
            self.repereEquilateral()
            vecteurOI = geom.vecteur(self.O, self.I)
            # Calcul des Points Utiles (centres,...) :
            centre = []
            centre.append(self.O)
            X = self.O.X + (vecteurOI.X / geom.SQRT_3)
            Y = self.O.Y + (vecteurOI.Y / geom.SQRT_3)
            P = geom.Point(X, Y)
            P = geom.rotation(P, self.O, geom.PI / 6)
            centre.append(P)
            P = geom.rotation(P, self.O, geom.PI / 3)
            centre.append(P)
            # Calcul des points du contour :
            self.s = (self.d - 1) * 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et les 2 autres centres de rotation :
            self.sommets[0] = self.O
            j = 2 + self.f[0] +  self.f[1]
            self.sommets[j] = centre[1]
            j = 4 + self.f[0] + 2 * self.f[1] +  self.f[2]
            self.sommets[j] = centre[2]
            # M et les 2 autres points correspondant :
            j = 1 + self.f[0]
            P = self.controlPoints[3]
            self.sommets[j] = P
            j = 3 + self.f[0] + 2 * self.f[1]
            P = geom.rotation(P, centre[1], 4 * geom.PI / 3)
            self.sommets[j] = P
            j = 5 + self.f[0] + 2 * self.f[1] + 2 * self.f[2]
            P = geom.rotation(P, centre[2], 4 * geom.PI / 3)
            self.sommets[j] = P
            # Les points déplaçables facultatifs et leurs images :
            k = 4
            j = 1
            l = self.s - 1
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[0].X, centre[0].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[0], 2 * geom.PI / 3)
            k = k + self.f[0]
            j = j + 1 + self.f[0]
            l = 2 + self.f[0] + 2 * self.f[1]
            for i in range(self.f[1]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[1].X, centre[1].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[1], 4 * geom.PI / 3)
            k = k + self.f[1]
            j = j + 2 + 2 * self.f[1]
            l = l + 2 + 2 * self.f[2]
            for i in range(self.f[2]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[2].X, centre[2].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[2], 4 * geom.PI / 3)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['rotation', self.O, 2 * geom.PI / 3])
            self.transformations.append(['rotation', self.O, 4 * geom.PI / 3])
        elif self.typePavage == 4:
            self.repereCarre()
            # Calcul des Points Utiles (centres,...) :
            centre = []
            centre.append(self.O)
            centre.append(geom.milieu(self.I, self.J))
            centre.append(geom.milieu(self.O, self.J))
            # Calcul des points du contour :
            self.s = (self.d - 1) * 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et les 2 autres centres :
            self.sommets[0] = self.O
            j = 2 + self.f[0] +  self.f[1]
            self.sommets[j] = centre[1]
            j = 4 + self.f[0] + 2 * self.f[1] +  self.f[2]
            self.sommets[j] = centre[2]
            # M et les 2 autres points correspondant :
            j = 1 + self.f[0]
            P = self.controlPoints[3]
            self.sommets[j] = P
            j = 5 + self.f[0] + 2 * self.f[1] + 2 * self.f[2]
            P = geom.rotation(P, centre[0], geom.PI / 2)
            self.sommets[j] = P
            j = 3 + self.f[0] + 2 * self.f[1]
            P = geom.rotation(P, centre[2], geom.PI)
            self.sommets[j] = P
            # Les points déplaçables facultatifs et leurs images :
            k = 4
            j = 1
            l = self.s - 1
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[0].X, centre[0].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[0], geom.PI / 2)
            k = k + self.f[0]
            j = j + 1 + self.f[0]
            l = 2 + self.f[0] + 2 * self.f[1]
            for i in range(self.f[1]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[1].X, centre[1].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[1], - geom.PI / 2)
            k = k + self.f[1]
            j = j + 2 + 2 * self.f[1]
            l = l + 2 + 2 * self.f[2]
            for i in range(self.f[2]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[2].X, centre[2].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[2], geom.PI)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['rotation', self.O,   geom.PI / 2])
            self.transformations.append(['rotation', self.O, 2 * geom.PI / 2])
            self.transformations.append(['rotation', self.O, 3 * geom.PI / 2])
        elif self.typePavage == 5:
            self.repereEquilateral()
            vecteurOI = geom.vecteur(self.O, self.I)
            # Calcul des Points Utiles (centres,...) :
            centre = []
            centre.append(self.O)
            centre.append(geom.milieu(self.O, self.I))
            X = self.O.X + (vecteurOI.X / geom.SQRT_3)
            Y = self.O.Y + (vecteurOI.Y / geom.SQRT_3)
            P = geom.Point(X, Y)
            P = geom.rotation(P, self.O, geom.PI / 6)
            centre.append(P)
            # Calcul des points du contour :
            self.s = (self.d - 1) * 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et les 2 autres centres :
            self.sommets[0] = self.O
            j = 2 + self.f[0] +  self.f[1]
            self.sommets[j] = centre[1]
            j = 4 + self.f[0] + 2 * self.f[1] +  self.f[2]
            self.sommets[j] = centre[2]
            # M et les 2 autres points correspondant :
            j = 1 + self.f[0]
            P = self.controlPoints[3]
            self.sommets[j] = P
            j = 3 + self.f[0] + 2 * self.f[1]
            P = geom.rotation(P, centre[1], geom.PI)
            self.sommets[j] = P
            j = 5 + self.f[0] + 2 * self.f[1] + 2 * self.f[2]
            P = geom.rotation(P, centre[2], -2 * geom.PI / 3)
            self.sommets[j] = P
            # Les points déplaçables facultatifs et leurs images :
            k = 4
            j = 1
            l = self.s - 1
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[0].X, centre[0].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[0], geom.PI / 3)
            k = k + self.f[0]
            j = j + 1 + self.f[0]
            l = 2 + self.f[0] + 2 * self.f[1]
            for i in range(self.f[1]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[1].X, centre[1].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[1], geom.PI)
            k = k + self.f[1]
            j = j + 2 + 2 * self.f[1]
            l = l + 2 + 2 * self.f[2]
            for i in range(self.f[2]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[2].X, centre[2].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[2], -2 * geom.PI / 3)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['rotation', self.O,   geom.PI / 3])
            self.transformations.append(['rotation', self.O, 2 * geom.PI / 3])
            self.transformations.append(['rotation', self.O, 3 * geom.PI / 3])
            self.transformations.append(['rotation', self.O, 4 * geom.PI / 3])
            self.transformations.append(['rotation', self.O, 5 * geom.PI / 3])
        elif self.typePavage == 6:
            self.repereRectangle()
            vecteurOI = geom.vecteur(self.O, self.I)
            # Calcul des Points Utiles (centres,...) :
            centre = []
            P = geom.Point(self.controlPoints[3].X, self.controlPoints[3].Y)
            P = geom.projectionOrthogonale(P, self.O, self.J)
            V = geom.vecteur(self.O, self.J)
            V.X = 0.5 * V.X
            V.Y = 0.5 * V.Y
            P = geom.translation(P, V)
            centre.append(P)
            # Recalcul du point M2, car la hauteur de OJ est double de celle de M1M2 :
            V = geom.vecteur(self.O, self.I)
            P = geom.translation(P, V)
            centre.append(P)
            M2 = geom.projectionOrthogonale(self.controlPoints[4], centre[0], centre[1])
            self.controlPoints[4].X = M2.X
            self.controlPoints[4].Y = M2.Y
            # Calcul des points du contour :
            self.s = (self.d - 2) * 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et I :
            self.sommets[0] = self.O
            j = 2 + self.f[0] +  self.f[1]
            self.sommets[j] = self.I
            # M1,M2 et les 2 autres points correspondant :
            j = 1 + self.f[0]
            P = self.controlPoints[3]
            self.sommets[j] = P
            P = geom.symetrieAxiale(P, self.O, self.J)
            V = geom.vecteur(P, self.controlPoints[4])
            P = geom.translation(self.O, V)
            j = 4 + self.f[0] + 2*self.f[1]+ self.f[2]
            self.sommets[j] = P
            j = 5 + 2 * self.f[0] + 2 * self.f[1] + self.f[2]
            P = self.controlPoints[4]
            self.sommets[j] = P
            P = geom.translation(P, vecteurOI)
            j = 3 + self.f[0] + self.f[1] + self.f[2]
            self.sommets[j] = P
            # Les points déplaçables facultatifs et leurs images :
            k = 5
            j = 1
            l = 5 + self.f[0] + 2 * self.f[1] + self.f[2]
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.O.X, self.O.Y
                self.sommets[j + i] = P
                self.sommets[l + i] = geom.glissage(P, self.O, self.J, V)
            k = k + self.f[0]
            j = j + 1 + self.f[0]
            l = 4 + self.f[0] + self.f[1] + self.f[2]
            for i in range(self.f[1]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.controlPoints[3].X, self.controlPoints[3].Y #self.I.X, self.I.Y
                self.sommets[j + i] = P
                P = geom.glissage(P, self.O, self.J, V)
                self.sommets[l + i] = geom.translation(P, vecteurOI)
            k = k + self.f[1]
            j = j + 1 + self.f[1]
            l = self.s - 1
            vecteurIO = geom.vecteur(self.I,self.O)
            for i in range(self.f[2]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.I.X, self.I.Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.translation(P, vecteurIO)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['glissage', self.O, self.J, V])
        elif self.typePavage == 7:
            self.repereRectangle()
            # Calcul des Points Utiles (centres,...) :
            centre = []
            centre.append(self.O)
            centre.append(geom.milieu(self.O, self.I))
            P = geom.milieu(self.I, self.J)
            P = geom.vecteur(self.O, P)
            centre.append(P)
            # Calcul des points du contour :
            self.s = (self.d - 1) * 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et le milieu de [OI] :
            self.sommets[0] = self.O
            j = 3 + self.f[0] + self.f[1] + self.f[2]
            self.sommets[j] = centre[1]
            #M1 et ses 2 images :
            j = 1 + self.f[0]
            M = self.controlPoints[3]
            self.sommets[j] = M
            j = 5 + self.f[0] + self.f[1] + 2 * self.f[2] + self.f[3]
            P = geom.glissage(M, self.O, self.J, centre[2])
            self.sommets[j] = P
            j = 7 + self.f[0] + 2 * self.f[1] + 2 * self.f[2] + 2 * self.f[3]
            P = geom.rotation(M, centre[0], geom.PI)
            self.sommets[j] = P
            centre.append(P)
            #M2 et ses 2 images :
            j = 2 + self.f[0] + self.f[1]
            M = self.controlPoints[4]
            self.sommets[j] = M
            j = 6 + self.f[0] + 2 * self.f[1] + 2 * self.f[2] + self.f[3]
            P = geom.glissage(M, self.O, self.J, centre[2])
            self.sommets[j] = P
            j = 4 + self.f[0] + self.f[1] + 2 * self.f[2]
            P = geom.rotation(M, centre[1], geom.PI)
            self.sommets[j] = P
            # Les points déplaçables facultatifs et leurs images :
            k = 5
            j = 1
            l = self.s - 1
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.O.X, self.O.Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[0], geom.PI)
            k = k + self.f[0]
            j = j + 1 + self.f[0]
            l = 6 + self.f[0] + self.f[1] + 2 * self.f[2] + self.f[3]
            for i in range(self.f[1]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.controlPoints[3].X, self.controlPoints[3].Y
                self.sommets[j + i] = P
                self.sommets[l + i] = geom.glissage(P, self.O, self.J, centre[2])
            k = k + self.f[1]
            j = j + 1 + self.f[1]
            l = 3 + self.f[0] + self.f[1] + 2 * self.f[2]
            for i in range(self.f[2]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[1].X, centre[1].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[1], geom.PI)
            k = k + self.f[2]
            j = j + 4 + 2 * self.f[2] + self.f[3] + self.f[1]
            l = 5 + self.f[0] + self.f[1] + 2 * self.f[2]
            for i in range(self.f[3]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[3].X, centre[3].Y
                self.sommets[j + i] = P
                self.sommets[l + i] = geom.glissage(P, self.O, self.I, centre[2])
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['glissage', self.O, self.J, centre[2]])
            self.transformations.append(['glissage', self.O, self.I, centre[2]])
            self.transformations.append(['rotation', centre[1], geom.PI])
        elif self.typePavage == 8:
            self.repereRectangle()
            vecteurOJ = geom.vecteur(self.O, self.J)
            # Calcul des Points Utiles (centres,...) :
            # Calcul des points du contour :
            self.s = (self.d - 2) * 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et J :
            self.sommets[0] = self.O
            j = 3 + 2 * self.f[0]
            self.sommets[j] = self.J
            #M1 et son image :
            #M1 doit être sur la médiatrice de [OI]
            P = geom.milieu(self.O, self.I)
            P2 = geom.translation(P, vecteurOJ)
            P = geom.projectionOrthogonale(self.controlPoints[3], P, P2)
            self.controlPoints[3].X = P.X
            self.controlPoints[3].Y = P.Y
            j = 1 + self.f[0]
            self.sommets[j] = self.controlPoints[3]
            j = 2 + self.f[0]
            P = geom.translation(self.controlPoints[3], vecteurOJ)
            self.sommets[j] = P
            # Les points déplaçables facultatifs et leurs images :
            k = 4
            j = 1
            l = 2 + 2 * self.f[0]
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.O.X, self.O.Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.translation(P, vecteurOJ)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', self.O, self.J])
        elif self.typePavage == 9:
            self.repereIsocele()
            vecteurOJ = geom.vecteur(self.O, self.J)
            # Calcul des Points Utiles (centres,...) :
            # Les milieux de [OI] et [OJ] :
            centre = []
            centre.append(geom.milieu(self.O, self.I))
            centre.append(geom.milieu(self.O, self.J))
            # Le vecteur qui les lie :
            vecteur1 = geom.vecteur(centre[0],centre[1])
            # Le vecteur IJ :
            vecteur2 = geom.vecteur(self.I,self.J)
            # Calcul des points du contour :
            self.s = 2 * self.d - 3
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et O' :
            self.sommets[0] = self.O
            j = self.s - 1
            self.sommets[j] = geom.translation(self.O, vecteur2)
            centre.append(self.sommets[j])
            # M et ses 2 images :
            j = 1 + self.f[0]
            P = self.controlPoints[3]
            self.sommets[j] = P
            j = 2 + self.f[0] + self.f[1]
            P = geom.glissage(P, centre[0], centre[1], vecteur1)
            self.sommets[j] = P
            j = 3 + self.f[0] + 2*self.f[1]
            P = self.controlPoints[3]
            P = geom.translation(P, vecteur2)
            self.sommets[j] = P
            # Les points déplaçables facultatifs et leurs images :
            k = 4
            j = 1
            l = self.s - 2
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.O.X, self.O.Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.translation(P, vecteur2)
            k = k + self.f[0]
            j = j + 1 + self.f[0]
            l = 3 + self.f[0] + self.f[1]
            for i in range(self.f[1]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.controlPoints[3].X, self.controlPoints[3].Y
                self.sommets[j + i] = P
                self.sommets[l + i] = geom.glissage(P, centre[0], centre[1], vecteur1)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', self.O, centre[2]])
        elif self.typePavage == 10:
            self.repereRectangle()
            vecteurOJ = geom.vecteur(self.O, self.J)
            # Calcul des Points Utiles (centres,...) :
            # Calcul des points du contour :
            self.s = self.d * 2 - 5
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et J :
            self.sommets[0] = self.O
            j = self.s - 1
            self.sommets[j] = self.J
            # M1 doit être sur la médiatrice de [OI]
            P = geom.milieu(self.O, self.I)
            P2 = geom.translation(P, vecteurOJ)
            P = geom.projectionOrthogonale(self.controlPoints[3], P, P2)
            self.controlPoints[3].X = P.X
            self.controlPoints[3].Y = P.Y
            j = 1 + 2 * self.f[0]
            self.sommets[j] = self.controlPoints[3]
            # Les milieux de [OM1] et [JM1]  ainsi que M1 :
            centre = []
            centre.append(geom.milieu(self.O, self.controlPoints[3]))
            centre.append(geom.milieu(self.J, self.controlPoints[3]))
            centre.append(self.controlPoints[3])
            # Les points déplaçables facultatifs et leurs images :
            k = 4
            j = 1
            l = 2 * self.f[0]
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.O.X, self.O.Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.symetrieCentrale(P, centre[0])
            k = k + self.f[0]
            j = 2 + 2 * self.f[0]
            l = self.s - 2
            for i in range(self.f[1]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.controlPoints[3].X, self.controlPoints[3].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.symetrieCentrale(P, centre[1])
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', self.O, self.J])
            self.transformations.append(['rotation', centre[0], geom.PI])
            self.transformations.append(['glissage', self.O, self.I, centre[2]])
        elif self.typePavage == 11:
            self.repereRectangle()
            # Calcul des Points Utiles (centres,...) :
            # Calcul des points du contour :
            self.s = 4
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O :
            self.sommets[0] = self.O
            # Les milieux de [OI] [IJ] et [OJ] :
            self.sommets[1] = geom.milieu(self.O, self.I)
            self.sommets[2] = geom.milieu(self.I, self.J)
            self.sommets[3] = geom.milieu(self.O, self.J)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', self.O, self.J])
            self.transformations.append(['symetrieAxiale', self.O, self.I])
            self.transformations.append(['rotation', self.O, geom.PI])
        elif self.typePavage == 12:
            self.repereIsocele()
            # Calcul des Points Utiles (centres,...) :
            # Les milieux de [IJ] et [OJ] :
            centre = []
            centre.append(geom.milieu(self.I, self.J))
            centre.append(geom.milieu(self.O, self.J))
            # Le point N :
            centre.append(geom.symetrieCentrale(centre[0], centre[1]))
            # Calcul des points du contour :
            self.s = self.d * 2 - 2
            for i in range(self.s):
                self.sommets.append(geom.Point())
            #O M R et N :
            self.sommets[0] = self.O
            self.sommets[1] = centre[0]
            j = 2 + self.f[0]
            self.sommets[j] = centre[1]
            j = self.s - 1
            self.sommets[j] = centre[2]
            # Les points déplaçables facultatifs et leurs images :
            k = 3
            j = 2
            l = self.s - 2
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = centre[0].X, centre[0].Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.symetrieCentrale(P, centre[1])
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', self.O, centre[2]])
            self.transformations.append(['rotation', self.O, geom.PI])
            self.transformations.append(['symetrieAxiale', self.O, centre[0]])
        elif self.typePavage == 13:
            self.repereCarre()
            # Calcul des Points Utiles (centres,...) :
            # Le centre du carré :
            centre = []
            centre.append(geom.milieu(self.I, self.J))
            # Les milieux A B C D des 4 côtés :
            centre.append(geom.milieu(self.O, self.I))
            centre.append(geom.rotation(centre[1], centre[0], geom.PI / 2))
            centre.append(geom.symetrieCentrale(centre[1], centre[0]))
            centre.append(geom.milieu(self.O, self.J))
            # Calcul des points du contour :
            self.s = self.d * 2 - 3
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O A et D :
            self.sommets[0] = self.O
            j = 1 + self.f[0]
            self.sommets[j] = centre[1]
            j = 2 + self.f[0]
            self.sommets[j] = centre[4]
            # Les points déplaçables facultatifs et leurs images :
            k = 3
            j = 1
            l = self.s - 1
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.O.X, self.O.Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, self.O, geom.PI / 2)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', centre[1], centre[4]])
            self.transformations.append(['symetrieAxialeEtRotation', centre[1], centre[4], centre[0], geom.PI / 2])
            self.transformations.append(['symetrieAxialeEtRotation', centre[1], centre[4], centre[0], geom.PI])
            self.transformations.append(['symetrieAxialeEtRotation', centre[1], centre[4], centre[0], 3 * geom.PI / 2])
            self.transformations.append(['rotation', centre[0], geom.PI / 2])
            self.transformations.append(['rotation', centre[0], geom.PI])
            self.transformations.append(['rotation', centre[0], 3 * geom.PI / 2])
        elif self.typePavage == 14:
            self.repereEquilateral()
            # Calcul des Points Utiles (centres,...) :
            # Calcul des points du contour :
            self.s = 3
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O :
            self.sommets[0] = self.O
            # Le centre de gravité du triangle OIJ :
            P = geom.milieu(self.I, self.J)
            P = geom.homothetie(P, self.O, 2/3)
            self.sommets[2] = P
            # Son symétrique par rapport à (OI) :
            P = geom.symetrieAxiale(P, self.O, self.I)
            self.sommets[1] = P
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', self.O, self.sommets[2]])
            self.transformations.append(['rotation', self.O, 2 * geom.PI / 3])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 2 * geom.PI / 3])
            self.transformations.append(['rotation', self.O, 4 * geom.PI / 3])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 4 * geom.PI / 3])
        elif self.typePavage == 15:
            self.repereEquilateral()   
            # Calcul des Points Utiles (centres,...) :
            # Le centre de gravité du triangle OIJ :
            centre = []
            P = geom.milieu(self.I, self.J)
            centre.append(geom.homothetie(P, self.O, 2/3))
            # Calcul des points du contour :
            self.s = self.d * 2 - 3
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O I et G :
            self.sommets[0] = self.O
            self.sommets[1] = self.I
            j = 2 + self.f[0]
            self.sommets[j] = centre[0]
            # Les points déplaçables facultatifs et leurs images :
            k = 3
            j = 2
            l = self.s - 1
            for i in range(self.f[0]):
                P = self.controlPoints[k + i]
                if P.X == MAGIC_NUMBER:
                    P.X , P.Y = self.I.X, self.I.Y
                self.sommets[j + i] = P
                self.sommets[l - i] = geom.rotation(P, centre[0], 4 * geom.PI / 3)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['rotation', centre[0], 2 * geom.PI / 3])
            self.transformations.append(['rotation', centre[0], 4 * geom.PI / 3])
            self.transformations.append(['symetrieAxiale', self.O, self.I])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.I, centre[0], 2 * geom.PI / 3])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.I, centre[0], 4 * geom.PI / 3])
        elif self.typePavage == 16:
            self.repereCarre()
            # Calcul des Points Utiles (centres,...) :
            # Calcul des points du contour :
            self.s = 3
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et les mileieux de [OI] et [IJ] :
            self.sommets[0] = self.O
            self.sommets[1] = geom.milieu(self.O, self.I)
            self.sommets[2] = geom.milieu(self.I, self.J)
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', self.O, self.sommets[2]])
            self.transformations.append(['rotation', self.O, 2 * geom.PI / 4])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 2 * geom.PI / 4])
            self.transformations.append(['rotation', self.O, 4 * geom.PI / 4])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 4 * geom.PI / 4])
            self.transformations.append(['rotation', self.O, 6 * geom.PI / 4])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 6 * geom.PI / 4])
        elif self.typePavage == 17:
            self.repereEquilateral()
            # Calcul des Points Utiles (centres,...) :
            # Calcul des points du contour :
            self.s = 3
            for i in range(self.s):
                self.sommets.append(geom.Point())
            # O et le milieu de [OI] :
            self.sommets[0] = self.O
            self.sommets[1] = geom.milieu(self.O, self.I)
            # Le centre de gravité du triangle OIJ :
            P = geom.milieu(self.I, self.J)
            P = geom.homothetie(P, self.O, 2/3)
            self.sommets[2] = P
            # L'Ensemble Fondamental de pavés :
            self.transformations.append(['identite'])
            self.transformations.append(['symetrieAxiale', self.O, self.sommets[2]])
            self.transformations.append(['rotation', self.O, 2 * geom.PI / 6])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 2 * geom.PI / 6])
            self.transformations.append(['rotation', self.O, 4 * geom.PI / 6])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 4 * geom.PI / 6])
            self.transformations.append(['rotation', self.O, 6 * geom.PI / 6])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 6 * geom.PI / 6])
            self.transformations.append(['rotation', self.O, 8 * geom.PI / 6])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 8 * geom.PI / 6])
            self.transformations.append(['rotation', self.O, 10 * geom.PI / 6])
            self.transformations.append(['symetrieAxialeEtRotation', self.O, self.sommets[2], self.O, 10 * geom.PI / 6])


