# PAVAGES

* **Site Web :** http://pascal.peter.free.fr/pavages.html
* **Email :** pascal.peter at free.fr
* **Licence :** GNU General Public License (version 3)
* **Copyright :** (c) 1998-2021

----

#### Les outils utilisés pour développer Pavages
* [Python](https://www.python.org) : langage de programmation
* [Qt](https://www.qt.io) : "toolkit" très complet (interface graphique et tout un tas de choses)
* [PyQt](https://riverbankcomputing.com) : lien entre Python et Qt

#### Autres bibliothèques utilisées et trucs divers
* [marked](https://github.com/chjj/marked) : pour afficher les fichiers Markdown

#### Traduction initiale en Anglais
* Olivier Aumaire (Olivieraumaire at aol.com)
* Florence Aumaire-Grevet

#### Divers
* [GNU GPL 3](http://www.gnu.org/copyleft/gpl.html) : licence publique générale GNU
