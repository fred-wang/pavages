<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/utils_about.py" line="103"/>
        <source>&amp;Close</source>
        <translation>ס&amp;גור</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="95"/>
        <source>About</source>
        <translation>אודות</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="98"/>
        <source>License</source>
        <translation>רשיון</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="514"/>
        <source>About this Application</source>
        <translation>אודות יישום זה</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="472"/>
        <source>Duplicate this Point</source>
        <translation>שכפל נקודה זו</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="479"/>
        <source>Delete this Point</source>
        <translation>מחק נקודה זו</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="545"/>
        <source>Create a new Point</source>
        <translation>צור נקודה חדשה</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="559"/>
        <source>Change the Tile Color</source>
        <translation>שנה את צבע האריח</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="481"/>
        <source>Line Style</source>
        <translation>סגנון הקו</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="483"/>
        <source>Set the Line Style</source>
        <translation>הגדר את סגנון הקו</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="485"/>
        <source>Select Brush</source>
        <translation>בחר מברשת</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="487"/>
        <source>Select the Brush</source>
        <translation>בחר את המברשת</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="540"/>
        <source>Create a Point on arc {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="550"/>
        <source>&amp;Colors</source>
        <translation>&amp;צבעים</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="554"/>
        <source>Color Tile {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="474"/>
        <source>A new Point will be create with the same position</source>
        <translation>תיווצר נקודה חדשה באותו המיקום</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="489"/>
        <source>Tiling Config</source>
        <translation>תצורת השיבוץ</translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="491"/>
        <source>Set the Tiling Config</source>
        <translation>הגדר את תצורת השיבוץ</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="426"/>
        <source>&amp;File</source>
        <translation>&amp;קובץ</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="506"/>
        <source>&amp;Help</source>
        <translation>ע&amp;זרה</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="423"/>
        <source>toolBarFile</source>
        <translation>toolBarFile</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="428"/>
        <source>&amp;Open</source>
        <translation>&amp;פתח</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="430"/>
        <source>Open a File</source>
        <translation>פתח קובץ</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="432"/>
        <source>&amp;Save</source>
        <translation>&amp;שמור</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="434"/>
        <source>Save the File</source>
        <translation>שמור את הקובץ</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="436"/>
        <source>Save &amp;as</source>
        <translation>שמור &amp;בשם</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="451"/>
        <source>&amp;Quit</source>
        <translation>י&amp;ציאה</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="453"/>
        <source>Quit Application</source>
        <translation>צא מהיישום</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="512"/>
        <source>&amp;About</source>
        <translation>&amp;אודות</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="458"/>
        <source>&amp;Brush</source>
        <translation>&amp;מברשת</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="503"/>
        <source>Export the Tiling as SVG File</source>
        <translation>ייצא את הריצוף כקובץ SVG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="481"/>
        <source>Export the Tile as PNG File</source>
        <translation>ייצא את האריח כקובץ PNG</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="438"/>
        <source>Save the File with a different name</source>
        <translation>שמור את הקובץ בשם אחר</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="441"/>
        <source>&amp;Language</source>
        <translation>&amp;שפה</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="443"/>
        <source>Choose Language</source>
        <translation>בחר שפה</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="90"/>
        <source>LanguageName</source>
        <translation>Hebrew</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="829"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="860"/>
        <source>Pavage Files (*.pav)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="860"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="471"/>
        <source>SVG Files (*.svg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="761"/>
        <source>PNG Files (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="791"/>
        <source>File must be saved ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="793"/>
        <source>Save ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_exports.py" line="376"/>
        <source>Cannot write file {0}:
{1}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="523"/>
        <source>p1</source>
        <translation>p1</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="523"/>
        <source>asymmetric parallelogramic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="525"/>
        <source>p2</source>
        <translation>p2</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="525"/>
        <source>symmetric parallelogramic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="527"/>
        <source>p3</source>
        <translation>p3</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="527"/>
        <source>hexagonal 3-rotative</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="529"/>
        <source>p4</source>
        <translation>p4</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="529"/>
        <source>square 4-rotative</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="531"/>
        <source>p6</source>
        <translation>p6</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="531"/>
        <source>hexagonal 6-rotative</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="533"/>
        <source>pg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="533"/>
        <source>gliding rectangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="535"/>
        <source>pgg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="535"/>
        <source>bi-gliding rectangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="537"/>
        <source>pm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="537"/>
        <source>mono-symmetric rectangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="539"/>
        <source>cm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="539"/>
        <source>mono-symmetric rhombic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="541"/>
        <source>pmg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="541"/>
        <source>symmetric gliding rectangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="543"/>
        <source>pmm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="543"/>
        <source>bi-symmetric rectangular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="545"/>
        <source>cmm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="545"/>
        <source>bi-symmetric rhombic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="547"/>
        <source>p4g</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="547"/>
        <source>gliding 4-rotative square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="549"/>
        <source>p3m1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="549"/>
        <source>tri-symmetric hexagonal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="551"/>
        <source>p31m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="551"/>
        <source>symmetric 3-rotative hexagonal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>p4m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="553"/>
        <source>totally symmetric square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="555"/>
        <source>p6m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="555"/>
        <source>totally symmetric hexagonal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="560"/>
        <source>Type {0}: {1} [{2}]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="568"/>
        <source>A context menu is available by right-clicking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="456"/>
        <source>E&amp;dit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="463"/>
        <source>Expor&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="276"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="465"/>
        <source>Export as PNG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="469"/>
        <source>Export as SVG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="479"/>
        <source>Export Tile as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="483"/>
        <source>Export Tile as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="485"/>
        <source>Export the Tile as SVG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="487"/>
        <source>Export Fundamental Set as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="489"/>
        <source>Export the Fundamental Set of Tiles as PNG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="492"/>
        <source>Export Fundamental Set as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="494"/>
        <source>Export the Fundamental Set of Tiles as SVG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="497"/>
        <source>Export Tiling as PNG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="499"/>
        <source>Export the Tiling as PNG File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="501"/>
        <source>Export Tiling as SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="508"/>
        <source>&amp;HelpPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="510"/>
        <source>Show the Help in your browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="516"/>
        <source>&amp;Explanations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="518"/>
        <source>Displaying explanations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="460"/>
        <source>Show the Brush</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="447"/>
        <source>To create a .desktop file launcher in the folder of your choice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="445"/>
        <source>Create a launcher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/views.py" line="98"/>
        <source>The tile is crossed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="607"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="193"/>
        <source>Cols:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="199"/>
        <source>Rows:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="65"/>
        <source>Pen &amp;Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="70"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="73"/>
        <source>Dash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="76"/>
        <source>Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="79"/>
        <source>Dash Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="82"/>
        <source>Dash Dot Dot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="85"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="89"/>
        <source>&amp;Pen Style:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="94"/>
        <source>Miter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="97"/>
        <source>Bevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="100"/>
        <source>Round</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="104"/>
        <source>Pen &amp;Join:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="108"/>
        <source>&amp;Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="110"/>
        <source>Pen Color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="277"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dialogs.py" line="281"/>
        <source>C&amp;ancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="116"/>
        <source>About {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="76"/>
        <source>(version {0})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="299"/>
        <source>&amp;Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="408"/>
        <source>No name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="473"/>
        <source>TILE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="475"/>
        <source>SET</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="477"/>
        <source>TILING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_exports.py" line="161"/>
        <source>Cannot open file {0}.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
