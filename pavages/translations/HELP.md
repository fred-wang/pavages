# Overview

### <a name="Pavage"></a>The tiling
The tiling is a regular filling of the plane, done with a single figure.

![](images/pavages_pavage.png)

The basic figure is the [tile](#Pave):

![](images/pavages_pave.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is a group of several tiles which allows to do the tiling in 2 directions of [translations](#Translations):

![](images/pavages_para.png)

----

### <a name="Ensemble_Fondamental"></a>Fundamental set of tiles
The fundamental set of tiles is a group of several [tiles](#Pave) which allows to do the [tiling](#Pavage) in 2 directions of [translations](#Translations).

![](images/pavages_para.png)

----

### <a name="Pave"></a>Tile
The tile is the basic element for a [tiling](#Pavage)

![](images/pavages_pave.png)

As a tile is in contact with another, each part of its **contour** appears twice.

![](images/pavages_t07_04.png)

----

### <a name="Groupe_Deplacements"></a>Group of displacement
Each type of tiling is associated to a [group](#Groupe) of displacement of the plane. This group is made of the displacement allowing to go from one tile to another.

The displacements are:
* The [translations](#Translations)
* The [rotations](#Rotations)
* The [central symmetries](#Symetries_Centrales)
* The [axial symmetries](#Symetries_Axiales)
* The [glide reflections](#Glissages)

The groups associated to a different type of tiling are always the results of a finite number of displacements, within which are 2 translations.

----

### <a name="Translations"></a>Translations
![](images/pavages_transf01.png)

In a translation, you displace the figure without turning it over or upside down.
<br/>The translation is represented by an arrow called **vector**.
<br/>The 2 translations associated to each [tiling](#Pavage) allow to go from one [fundamental set of tiles](#Ensemble_Fondamental) to another.

![](images/pavages_transf06.png)

----

### <a name="Rotations"></a>Rotations
![](images/pavages_transf02.png)

In a rotation, the figure turns around a point called **center**.
<br/>It is also necessary to know the **angle** of rotation.
<br/>The rotations associated to a [tiling](#Pavage) always have their angles being equal to:
* 1/6 of rotation and multiple angles (2/6=1/3, 3/6=1/2, 4/6=2/3 and 5/6)
<br/>![](images/pavages_transf07.png)
* 1/4 of rotation and multiple angles (2/4=1/2 and 3/4)
<br/>![](images/pavages_transf08.png)
* 1/3 of rotation and multiple angles (2/3)
<br/>![](images/pavages_transf09.png)
* 1/2 of rotation (these are central symmetries)
<br/>![](images/pavages_transf10.png)

The centers of rotation are always on the **contour** of the tile:

![](images/pavages_transf11.png)

----

### <a name="Symetries_Centrales"></a>Central symmetries
![](images/pavages_transf03.png)

A central symmetry is a [rotation](#Rotations) the angle of which is a 1/2 turn.

----

### <a name="Symetries_Axiales"></a>Axial symmetries
![](images/pavages_transf04.png)

In an axial symmetry, the figure is turned around an **axis**.
<br/>When a [tiling](#Pavage) has an axis of symmetry, a part of the **contour** of the tile is along this axis and cannot be distorted.

![](images/pavages_transf12.png)

The tilings with several directions of angles of symmetry, often have a fixed contour:

![](images/pavages_transf13.png)

----

### <a name="Glissages"></a>Glide reflections
![](images/pavages_transf05.png)

You can get a glide reflection if you compose an [axial symmetry](#Symetries_Axiales) with a [translation](#Translations) the **vector** of which is parallel to the axis of symmetry.

----

### <a name="Groupe"></a>Group
A group is a set (let us call it G) with a law of internal composition (if x and y are parts of G , x¤y too) associative (x ¤ y) ¤ z = x ¤ (y ¤ z),We can then write x¤y¤z) as a neutral element exists (e is the neutral element if, for each x, we’ve got x ¤ e = e ¤ x = x) and if each element has an inverse (reciprocal) (for each x , y exists as x ¤ y = y ¤ x = e).

**Examples:**
* the integers (positive or negative) form a group for addition (the neutral element is zero and the opposite (additive inverse) of 5 is -5
* however, positive integers , don’t form a group for addition
* the displacements of the plane form a group for [composition](#Composition).

----

### <a name="Composition"></a>Composition
The composition of two displacements of the plane consists in doing two displacements one after the other.

**Example:**
* The composition of two translations is a translation:
<br/>![](images/pavages_transf14.png)
* The composition of a translation and a rotation is a rotation:
<br/>![](images/pavages_transf15.png)
* The composition of two axial symmetries is a translation or a rotation:
<br/>![](images/pavages_transf16.png)![](images/pavages_transf17.png)

----




# The 17 types of tilings

### <a name="type1"></a>Type nb 1: p1
### (asymmetric parallelogramic)
![](images/pavages_t01_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of a single [tile](#Pave):

![](images/pavages_t01_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by only two [translations](#Translations).

The **contour** of the tile has got 2 parts:

![](images/pavages_t01_04.png)

----

### <a name="type2"></a>Type nb 2: p2
### (symmetric parallelogramic)
![](images/pavages_t02_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 2 [tiles](#Pave):

![](images/pavages_t02_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* a [central symmetry](#Symetries_Centrales)

![](images/pavages_t02_03.png)

The **contour** of the tile has got 4 parts:

![](images/pavages_t02_04.png)

----

### <a name="type3"></a>Type nb 3: p3
### (hexagonal 3-rotative)
![](images/pavages_t03_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 3 [tiles](#Pave) and both the vectors form an equilateral triangle:

![](images/pavages_t03_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* a [rotation](#Rotations) (1/3 of a turn)

![](images/pavages_t03_03.png)

The **contour** of the tile has got 3 parts:

![](images/pavages_t03_04.png)

----

### <a name="type4"></a>Type nb 4: p4
### (square 4-rotative)
![](images/pavages_t04_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 4 [tiles](#Pave), and both the vectors form a half square:

![](images/pavages_t04_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* a [rotation](#Rotations) (1/4 of a turn)

![](images/pavages_t04_03.png)

The **contour** of the tile has got 3 parts:

![](images/pavages_t04_04.png)

----

### <a name="type5"></a>Type nb 5: p6
### (hexagonal 6-rotative)
![](images/pavages_t05_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 6 [tiles](#Pave), and both the vectors form an equilateral triangle:

![](images/pavages_t05_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* a [rotation](#Rotations) (1/6 of a turn)

![](images/pavages_t05_03.png)

The **contour** of the tile has got 3 parts:

![](images/pavages_t05_04.png)

----

### <a name="type6"></a>Type nb 6: pg
### (gliding rectangular)
![](images/pavages_t06_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 2 [tiles](#Pave), and both the vectors form a half rectangle:

![](images/pavages_t06_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* a [glide reflection](#Glissages)

![](images/pavages_t06_03.png)

The **contour** of the tile has got 3 parts:

![](images/pavages_t06_04.png)

----

### <a name="type7"></a>Type nb 7: pgg
### (bi-gliding rectangular)
![](images/pavages_t07_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 4 [tiles](#Pave), and both the vectors form a half rectangle:

![](images/pavages_t07_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* a [central symmetry](#Symetries_Centrales)
* 2 [glide reflections](#Glissages)

![](images/pavages_t07_03.png)

The **contour** of the tile has got 4 parts:

![](images/pavages_t07_04.png)

----

### <a name="type8"></a>Type nb 8: pm
### (mono-symmetric rectangular)
![](images/pavages_t08_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 2 [tiles](#Pave), and both the vectors form a half rectangle:

![](images/pavages_t08_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* an [axial symmetry](#Symetries_Axiales)

![](images/pavages_t08_03.png)

The **contour** of the tile has got 2 parts:

![](images/pavages_t08_04.png)

----

### <a name="type9"></a>Type nb 9: cm
### (mono-symmetric rhombic)
![](images/pavages_t09_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 2 [tiles](#Pave), and both the vectors form an isosceles triangle:

![](images/pavages_t09_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* an [axial symmetry](#Symetries_Axiales)

![](images/pavages_t09_03.png)

The **contour** of the tile has got 3 parts:

![](images/pavages_t09_04.png)

----

### <a name="type10"></a>Type nb 10: pmg
### (symmetric gliding
rectangular)
![](images/pavages_t10_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 4 [tiles](#Pave), and both the vectors form a half rectangle:

![](images/pavages_t10_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* a [central symmetry](#Symetries_Centrales)
* 2 [axial symmetries](#Symetries_Axiales)

![](images/pavages_t10_03.png)

The **contour** of the tile has got 3 parts:

![](images/pavages_t10_04.png)

----

### <a name="type11"></a>Type nb 11: pmm
### (bi-symmetric rectangular)
![](images/pavages_t11_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 4 [tiles](#Pave), and both the vectors form a half rectangle:

![](images/pavages_t11_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* 2 [axial symmetries](#Symetries_Axiales)

![](images/pavages_t11_03.png)

The **contour** of the tile is a rectangle.

----

### <a name="type12"></a>Type nb 12: cmm
### (bi-symmetric rhombic)
![](images/pavages_t12_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 4 [tiles](#Pave), and both the vectors form an isosceles triangle:

![](images/pavages_t12_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* 2 [axial symmetries](#Symetries_Axiales)

![](images/pavages_t12_03.png)

The **contour** of the tile has got 3 parts:

![](images/pavages_t12_04.png)

----

### <a name="type13"></a>Type nb 13: p4g
### (gliding 4-rotative square)
![](images/pavages_t13_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 8 [tiles](#Pave), and both the vectors form a half square:

![](images/pavages_t13_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* an [axial symmetry](#Symetries_Axiales)
* a [rotation](#Rotations) (1/4 of a turn)

![](images/pavages_t13_03.png)

The **contour** of the tile has got 2 parts:

![](images/pavages_t13_04.png)

----

### <a name="type14"></a>Type nb 14: p3m1
### (tri-symmetric hexagonal)
![](images/pavages_t14_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 6 [tiles](#Pave), and both the vectors form an equilateral triangle:

![](images/pavages_t14_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* an [axial symmetry](#Symetries_Axiales)
* a [rotation](#Rotations) (1/3 of a turn)

![](images/pavages_t14_03.png)

The **contour** of the tile is an equilateral triangle.

----

### <a name="type15"></a>Type nb 15: p31m
### (symmetric 3-rotative hexagonal)
![](images/pavages_t15_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 6 [tiles](#Pave), and both the vectors form an equilateral triangle:

![](images/pavages_t15_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* an [axial symmetry](#Symetries_Axiales)
* a [rotation](#Rotations) (1/3 of a turn)

![](images/pavages_t15_03.png)

The **contour** of the tile has got 2 parts:

![](images/pavages_t15_04.png)

----

### <a name="type16"></a>Type nb 16: p4m
### (totally symmetric square)
![](images/pavages_t16_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 8 [tiles](#Pave), and both the vectors form a half square:

![](images/pavages_t16_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* an [axial symmetry](#Symetries_Axiales)
* a [rotation](#Rotations) (1/4 of a turn)

![](images/pavages_t16_03.png)

The **contour** of the tile is a right and isosceles triangle (half square).

----

### <a name="type17"></a>Type nb 17: p6m
### (totally symmetric hexagonal)
![](images/pavages_t17_01.png)

The [fundamental set of tiles](#Ensemble_Fondamental) is made of 12 [tiles](#Pave), and both the vectors form an equilateral triangle:

![](images/pavages_t17_02.png)

The [group of displacement](#Groupe_Deplacements) is generated by
* 2 [translations](#Translations)
* an [axial symmetry](#Symetries_Axiales)
* a [rotation](#Rotations) (1/6 of a turn)

![](images/pavages_t17_03.png)

The **contour** of the tile is a right triangle, the angles are 30°, 60° and 90°.


