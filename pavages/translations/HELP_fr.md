# Généralités

### <a name="Pavage"></a>Pavage
Un pavage est un remplissage régulier du plan, réalisé avec une figure unique.

![](images/pavages_pavage.png)

La figure de base s'appelle le [pavé](#Pave) :

![](images/pavages_pave.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est un groupe de plusieurs pavés qui permet de réaliser le pavage avec deux directions de [translations](#Translations) :

![](images/pavages_para.png)

----

### <a name="Ensemble_Fondamental"></a>Ensemble Fondamental de Pavés
L'ensemble fondamental de pavés est un groupe de plusieurs [pavés](#Pave) qui permet de réaliser le [pavage](#Pavage) avec deux directions de [translations](#Translations) :

![](images/pavages_para.png)

----

### <a name="Pave"></a>Pavé
Le pavé est l'élément de base d'un [pavage](#Pavage).

![](images/pavages_pave.png)

Comme un pavé est en contact avec d'autres pavés, chaque partie de son **contour** se retrouve 2 fois :

![](images/pavages_t07_04.png)

----

### <a name="Groupe_Deplacements"></a>Groupe de Déplacements
Chaque type de pavage est associé à un [groupe](#Groupe) de déplacements du plan. Ce groupe est constitué par les déplacements permettant de passer d'un pavé à un autre.

Les déplacements sont :
* les [translations](#Translations)
* les [rotations](#Rotations)
* les [symétries centrales](#Symetries_Centrales)
* les [symétries axiales](#Symetries_Axiales)
* les [glissages](#Glissages)

Les groupes associés aux différents types de pavages sont toujours engendrés par un nombre fini de déplacements, dont 2 translations.

----

### <a name="Translations"></a>Translations
![](images/pavages_transf01.png)

Dans une translation, on déplace la figure sans la tourner ni la retourner.
<br/>La translation est représentée par une flèche appelée **vecteur**.
<br/>Les 2 translations associées à tout [pavage](#Pavage) permettent de passer d'un [ensemble fondamental de pavés](#Ensemble_Fondamental) à un autre :

![](images/pavages_transf06.png)

----

### <a name="Rotations"></a>Rotations
![](images/pavages_transf02.png)

Dans une rotation, la figure tourne autour d'un point appelé **centre**.
<br/>On doit aussi indiquer l'**angle** de la rotation.
<br/>Les rotations associées à un [pavage](#Pavage) ont toujours leurs angles égaux à :
* 1/6 de tour et les angles multiples (2/6=1/3, 3/6=1/2, 4/6=2/3 et 5/6)
<br/>![](images/pavages_transf07.png)
* 1/4 de tour et les angles multiples (2/4=1/2 et 3/4)
<br/>![](images/pavages_transf08.png)
* 1/3 de tour et les angles multiples (2/3)
<br/>![](images/pavages_transf09.png)
* 1/2 tour (ce sont des symétries centrales)
<br/>![](images/pavages_transf10.png)

Les centres des rotations sont toujours sur le **contour** du pavé :

![](images/pavages_transf11.png)

----

### <a name="Symetries_Centrales"></a>Symétries Centrales
![](images/pavages_transf03.png)

Une symétrie centrale est une [rotation](#Rotations) dont l'angle est 1/2 tour.

----

### <a name="Symetries_Axiales"></a>Symétries Axiales
![](images/pavages_transf04.png)

Dans une symétrie axiale, la figure est retournée autour d'un **axe**.
<br/>Lorsqu'un [pavage](#Pavage) possède un axe de symétrie, une partie du **contour** du pavé est située le long de cet axe, et ne peut donc être déformée :

![](images/pavages_transf12.png)

Les pavages possédant plusieurs directions d'axes de symétrie ont souvent un contour fixe :

![](images/pavages_transf13.png)

----

### <a name="Glissages"></a>Glissages
![](images/pavages_transf05.png)

Un glissage est obtenu en composant une **symétrie axiale** avec une [translation](#Translations) dont le **vecteur** est parallèle à l'axe de la symétrie.

----

### <a name="Groupe"></a>Groupe
Un groupe est un ensemble (notons-le G) muni d'une loi de composition interne (si x et y sont éléments de G, x ¤ y aussi) associative ((x ¤ y) ¤ z = x ¤ (y ¤ z), et on peut donc écrire x ¤ y ¤ z) telle qu'il existe un élément neutre (e est l'élément neutre si, pour tout x, on a x ¤ e = e ¤ x = x) et que tout élément ait un inverse (pour tout x, il existe y tel que x ¤ y = y ¤ x = e).

**Exemples :**
* les nombres entiers relatifs (positifs et négatifs) forment un groupe pour l'addition (l'élément neutre est 0, et l'inverse de 5 est -5)
* par contre, les nombres entiers positifs seuls ne forment pas un groupe pour l'addition
* les déplacements du plan forment un groupe pour la [composition](#Composition).

----

### <a name="Composition"></a>Composition
La composition de 2 déplacements du plan consiste à effectuer les 2 déplacements l'un après l'autre.

**Exemples :**
* La composition de 2 translations donne une translation
<br/>![](images/pavages_transf14.png)
* La composition d'une rotation et d'une translation donne une rotation
<br/>![](images/pavages_transf15.png)
* La composition de 2 symétries axiales donne une translation ou une rotation
<br/>![](images/pavages_transf16.png)![](images/pavages_transf17.png)

----




# Les 17 types de pavages

### <a name="type1"></a>Type n°1 : p1
### (parallélogrammique asymétrique)
![](images/pavages_t01_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) n'est constitué que d'un seul [pavé](#Pave) :

![](images/pavages_t01_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) n'est engendré que par 2 [translations](#Translations).

Le **contour** du pavé possède 2 parties :

![](images/pavages_t01_04.png)

----

### <a name="type2"></a>Type n°2 : p2
### (parallélogrammique symétrique)
![](images/pavages_t02_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 2 [pavés](#Pave) :

![](images/pavages_t02_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie centrale](#Symetries_Centrales)

![](images/pavages_t02_03.png)

Le **contour** du pavé possède 4 parties :

![](images/pavages_t02_04.png)

----

### <a name="type3"></a>Type n°3 : p3
### (héxagonal 3-rotatif)
![](images/pavages_t03_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 3 [pavés](#Pave), et les 2 vecteurs forment un triangle équilatéral :

![](images/pavages_t03_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [rotation](#Rotations) (1/3 de tour)

![](images/pavages_t03_03.png)

Le **contour** du pavé possède 3 parties :

![](images/pavages_t03_04.png)

----

### <a name="type4"></a>Type n°4 : p4
### (carré 4-rotatif)
![](images/pavages_t04_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 4 [pavés](#Pave), et les 2 vecteurs forment la moitié d'un carré :

![](images/pavages_t04_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [rotation](#Rotations) (1/4 de tour)

![](images/pavages_t04_03.png)

Le **contour** du pavé possède 3 parties :

![](images/pavages_t04_04.png)

----

### <a name="type5"></a>Type n°5 : p6
### (héxagonal 6-rotatif)
![](images/pavages_t05_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 6 [pavés](#Pave), et les 2 vecteurs forment un triangle équilatéral :

![](images/pavages_t05_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [rotation](#Rotations) (1/6 de tour)

![](images/pavages_t05_03.png)

Le **contour** du pavé possède 3 parties :

![](images/pavages_t05_04.png)

----

### <a name="type6"></a>Type n°6 : pg
### (rectangulaire glissant)
![](images/pavages_t06_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 2 [pavés](#Pave), et les 2 vecteurs forment la moitié d'un rectangle :

![](images/pavages_t06_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* un [glissage](#Glissages)

![](images/pavages_t06_03.png)

Le **contour** du pavé possède 3 parties :

![](images/pavages_t06_04.png)

----

### <a name="type7"></a>Type n°7 : pgg
### (rectangulaire biglissant)
![](images/pavages_t07_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 4 [pavés](#Pave), et les 2 vecteurs forment la moitié d'un rectangle :

![](images/pavages_t07_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie centrale](#Symetries_Centrales)
* 2 [glissages](#Glissages)

![](images/pavages_t07_03.png)

Le **contour** du pavé possède 4 parties :

![](images/pavages_t07_04.png)

----

### <a name="type8"></a>Type n°8 : pm
### (rectangulaire monosymétrique)
![](images/pavages_t08_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 2 [pavés](#Pave), et les 2 vecteurs forment la moitié d'un rectangle :

![](images/pavages_t08_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie axiale](#Symetries_Axiales)

![](images/pavages_t08_03.png)

Le **contour** du pavé possède 2 parties :

![](images/pavages_t08_04.png)

----

### <a name="type9"></a>Type n°9 : cm
### (rhombique monosymétrique)
![](images/pavages_t09_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 2 [pavés](#Pave), et les 2 vecteurs forment un triangle isocèle :

![](images/pavages_t09_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie axiale](#Symetries_Axiales)

![](images/pavages_t09_03.png)

Le **contour** du pavé possède 3 parties :

![](images/pavages_t09_04.png)

----

### <a name="type10"></a>Type n°10 : pmg
### (rectangulaire glissant symétrique)
![](images/pavages_t10_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 4 [pavés](#Pave), et les 2 vecteurs forment la moitié d'un rectangle :

![](images/pavages_t10_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie centrale](#Symetries_Centrales)
* 2 [symétries axiales](#Symetries_Axiales)

![](images/pavages_t10_03.png)

Le **contour** du pavé possède 3 parties :

![](images/pavages_t10_04.png)

----

### <a name="type11"></a>Type n°11 : pmm
### (rectangulaire bisymétrique)
![](images/pavages_t11_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 4 [pavés](#Pave), et les 2 vecteurs forment la moitié d'un rectangle :

![](images/pavages_t11_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* 2 [symétries axiales](#Symetries_Axiales)

![](images/pavages_t11_03.png)

Le **contour** du pavé est un rectangle.

----

### <a name="type12"></a>Type n°12 : cmm
### (rhombique bisymétrique)
![](images/pavages_t12_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 4 [pavés](#Pave), et les 2 vecteurs forment un triangle isocèle :

![](images/pavages_t12_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* 2 [symétries axiales](#Symetries_Axiales)

![](images/pavages_t12_03.png)

Le **contour** du pavé possède 3 parties :

![](images/pavages_t12_04.png)

----

### <a name="type13"></a>Type n°13 : p4g
### (carré 4-rotatif glissant)
![](images/pavages_t13_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 8 [pavés](#Pave), et les 2 vecteurs forment la moitié d'un carré :

![](images/pavages_t13_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie axiale](#Symetries_Axiales)
* une [rotation](#Rotations) (1/4 de tour)

![](images/pavages_t13_03.png)

Le **contour** du pavé possède 2 parties :

![](images/pavages_t13_04.png)

----

### <a name="type14"></a>Type n°14 : p3m1
### (héxagonal trisymétrique)
![](images/pavages_t14_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 6 [pavés](#Pave), et les 2 vecteurs forment un triangle équilatéral :

![](images/pavages_t14_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie axiale](#Symetries_Axiales)
* une [rotation](#Rotations) (1/3 de tour)

![](images/pavages_t14_03.png)

Le **contour** du pavé est un triangle équilatéral.

----

### <a name="type15"></a>Type n°15 : p31m
### (héxagonal 3-rotatif symétrique)
![](images/pavages_t15_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 6 [pavés](#Pave), et les 2 vecteurs forment un triangle équilatéral :

![](images/pavages_t15_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie axiale](#Symetries_Axiales)
* une [rotation](#Rotations) (1/3 de tour)

![](images/pavages_t15_03.png)

Le **contour** du pavé possède 2 parties :

![](images/pavages_t15_04.png)

----

### <a name="type16"></a>Type n°16 : p4m
### (carré totalement symétrique)
![](images/pavages_t16_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 8 [pavés](#Pave), et les 2 vecteurs forment la moitié d'un carré :

![](images/pavages_t16_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie axiale](#Symetries_Axiales)
* une [rotation](#Rotations) (1/4 de tour)

![](images/pavages_t16_03.png)

Le **contour** du pavé est un triangle rectangle et isocèle (demi-carré).

----

### <a name="type17"></a>Type n°17 : p6m
### (héxagonal totalement symétrique)
![](images/pavages_t17_01.png)

L'[ensemble fondamental de pavés](#Ensemble_Fondamental) est constitué de 12 [pavés](#Pave), et les 2 vecteurs forment un triangle équilatéral :

![](images/pavages_t17_02.png)

Le [groupe de déplacements](#Groupe_Deplacements) est engendré par
* 2  [translations](#Translations)
* une [symétrie axiale](#Symetries_Axiales)
* une [rotation](#Rotations) (1/6 de tour)

![](images/pavages_t17_03.png)

Le **contour** du pavé est un triangle rectangle d'angle 30°, 60° et 90°.

